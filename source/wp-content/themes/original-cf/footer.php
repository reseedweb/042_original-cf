</div><!-- .row -->
</div><!-- .container -->

<footer id="footer">
<div class="footer_bg">
<div class="container">
	<div class="row creafix">
  <div class="pull-left">
  <p class="pt20 pr30"><img src="<?php bloginfo('template_url'); ?>/img/footer_rebecca_logo.png" alt="レベッカのロゴ"></p>
  </div><!-- / -->
  
  <div class="pull-left">
  <p class="mr20 ml30"><img src="<?php bloginfo('template_url'); ?>/img/footer_logo.jpg" alt="オリジナルクリアファイルWebロゴ"></p>
  </div><!-- / -->
  
  <div class="">
  <p class="mb5"><img src="<?php bloginfo('template_url'); ?>/img/footer_tel.png" alt="電話番号:0120-838-117"></p>
  <p class="pr5 pull-left"><a href="<?php bloginfo('url'); ?>/contact"><img src="<?php bloginfo('template_url'); ?>/img/footer_con.png" alt="お問い合わせはこちら"></a></p>
  <p><a href="<?php bloginfo('url'); ?>/estimation"><img src="<?php bloginfo('template_url'); ?>/img/footer_est.png" alt="お見積もりはこちら"></a></p>
  </div><!-- / -->

	</div><!-- /row -->

</div>
</div>

<div class="container">
<div class="row footInner">
		<div class="col-xs-3">
			<h4>オーダーガイド</h4>
      <ul class="list-unstyled">
      <li><a href="<?php bloginfo('url'); ?>/shape">形状を選ぶ</a></li>
      <li><a href="<?php bloginfo('url'); ?>/size">サイズを選ぶ</a></li>
      <li><a href="<?php bloginfo('url'); ?>/material">素材を選ぶ</a></li>
      <li><a href="<?php bloginfo('url'); ?>/print">印刷方法を選ぶ</a></li>
      <li><a href="<?php bloginfo('url'); ?>/option">オプションを選ぶ</a></li>
      </ul>
		</div>
		<div class="col-xs-3">
			<h4>ご利用案内</h4>
      <ul class="list-unstyled">
      <li><a href="<?php bloginfo('url'); ?>/about">初めての方へ</a></li>
      <li><a href="<?php bloginfo('url'); ?>/price">参考価格表</a></li>
      <li><a href="<?php bloginfo('url'); ?>/example">制作事例集</a></li>
      <li><a href="<?php bloginfo('url'); ?>/flow">ご注文の流れ</a></li>
      <li><a href="<?php bloginfo('url'); ?>/draft">データ入稿について</a></li>
      <li><a href="<?php bloginfo('url'); ?>/estimation">お見積り依頼</a></li>
      </ul>
		</div>
		<div class="col-xs-3">
			<h4>特集コンテンツ</h4>
      <ul class="list-unstyled">
      <li><a href="<?php bloginfo('url'); ?>/fullorder">フルオーダー</a></li>
      <li><a href="<?php bloginfo('url'); ?>/nameorder">名入れオーダー</a></li>
      <li><a href="<?php bloginfo('url'); ?>/design">オリジナルデザイン制作</a></li>
      <li><a href="<?php bloginfo('url'); ?>/about#novelty">ノベルティグッズ制作</a></li>
      </ul>
		</div>
		<div class="col-xs-3">
			<h4>企業案内</h4>
      <ul class="list-unstyled">
      <li><a href="<?php bloginfo('url'); ?>/faq">よくあるご質問</a></li>
      <li><a href="http://rereca.com/about/" target="_blank">会社案内</a></li>
      <li><a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー</a></li>
      <li><a href="<?php bloginfo('url'); ?>/tradelaw">特定商取引法に基づく記載</a></li>
      <li><a href="<?php bloginfo('url'); ?>/contact">お問い合わせ</a></li>
      </ul>
		</div>
		</div>
<div class="clearfix">

  <p class="pull-left mr20 ml90"><a target="_blank" href="http://original-eb.net"><img src="<?php bloginfo('template_url'); ?>/img/side_eb_banner.jpg" alt="オリジナルエコバッグWeb"></a></p>
  <p class="pull-left mr20"><a target="_blank" href="http://original-db.net"><img src="<?php bloginfo('template_url'); ?>/img/side_db_banner.jpg" alt="オリジナル宅配袋Web"></a></p>
  <p><a target="_blank" href="http://original-sb.net"><img src="<?php bloginfo('template_url'); ?>/img/side_sb_banner.jpg" alt="オリジナルショッパーWeb" width="279" height="115"></a></p>


</div>


</div>
</div>


<div class="container text-center pv20">
<small>Copyright &copy; Original clear file Web. All Rights Reserved.</small>
</div>
</footer><!-- end of #footer -->

<!--[if lt IE 9]>
//IE8以下のHTML5とレスポンシブ対応
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js" type="text/javascript"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" type="text/javascript"></script>
<![endif]-->
<!--bootstrap minified JavaScriptの読み込み-->

<div id="mytest">
  <p>こちらの電話番号をタップしていただくと直接お電話可能です。</p>
  <p><a href="tel:0120838117" id="telbutton"><img src="<?php bloginfo('template_url'); ?>/img/smartphone_btn.jpg" hight="auto" width="100%"></a></p>
</div>

<!-- class="pageTop" -->

<script type="text/javascript">
  $('#telbutton').on('click', function(){
    ga('send','event','smartphone','tel');
  });
</script>
</body>
</html>
<?php wp_footer(); ?>