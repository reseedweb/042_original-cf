<?php
/*
Template Name: nameOrder
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">
  
<?php get_template_part('part','bread'); ?>				


  <h2 class="content_h2 mt0"><i class="fa fa-circle-o"></i>企業名・ロゴの名入れ印刷が可能です！</h2>

  <div class="cf">
  <img src="<?php bloginfo('template_url'); ?>/img/img_nameorder01.jpg" class="mr20 pb20 pull-left" alt="">
  <p class="pull-right fullorder_txt01">企業名やロゴを印刷したクリアファイルを安価で作りたい方には、名入れオーダーがお勧めです
！フルオーダーよりも選べる内容は減りますが、より手軽に安価に短納期で制作することができます。名入れ印刷のクリアファイルは企業のノベルティグッズ・展示会や説明会での配布用・販促グッズとして定番のグッズです。<br />
より高級感を出したい方は箔押しがお勧めです！箔の色は８種類のなかからお好きなものをお選びいただけます。</p>
  </div><!-- /cf -->
  
  <div class="cf">
	<figure class="pb20 pull-left">
  <img src="<?php bloginfo('template_url'); ?>/img/img_nameorder02.jpg" alt="企業の販促グッズ・資料配布用に！">
  <figcaption class="mt10">
  企業の販促グッズ・資料配布用に！
  </figcaption>
  </figure>

	<figure class="pb20 pull-left">
  <img src="<?php bloginfo('template_url'); ?>/img/img_nameorder03.jpg" alt="サイズは自由に選べます！">
  <figcaption class="mt10">
  サイズは自由に選べます！
  </figcaption>
  </figure>

	<figure class="mr0 pb20 pull-left">
  <img src="<?php bloginfo('template_url'); ?>/img/img_nameorder04.jpg" alt="箔押しで高級感アップです！">
  <figcaption class="mt10">
  箔押しで高級感アップです！
  </figcaption>
  </figure>

  </div><!-- /cf -->

  <h3 class="mt30">名入れオーダーはこんな方にお勧めです！</h3>
  <div class="cf pt20 fullorder_txt02">
  <img src="<?php bloginfo('template_url'); ?>/img/img_nameorder05.jpg" class="mr20 pb20 pull-left" alt="">
  <p class="red pull-right">■ 小ロットでも安価に制作したい！</p>
  <p class="pull-right">名入れクリアファイルは最小ロット100枚からご注文を承っております。小ロットでも安価で制作いただけます。
個人様からのご依頼もお待ちしております。お気軽にご相談ください。</p>
  <p class="red pull-right">■ 名入れだけ印刷でも安っぽくしたくない！</p>
  <p class="pull-right">ロゴや企業名のみ印刷して制作費用を抑えたいが、安っぽくなるのはイヤ…そんなお客様には「箔押し」がお勧めです。光沢のある箔でホッとスタンプし、高級感のある仕上がりです。箔の色もお選びいただけます。</p>
  <p class="red pull-right">■ イベントでの配布用・販促グッズに<br />名入れグッズを作りたい</p>
  <p class="pull-right">名入れクリアファイルはイベント資料やご案内と一緒に配布できて、実用的で日常的に長く使っていただけるものなので宣伝効果も高い商品です。販促物として名入れクリアファイルはイチオシです！</p>
  </div>
  
  
  
  <h2 class="content_h2"><i class="fa fa-circle-o"></i>はじめての方でも選ぶだけで簡単お見積もり！</h2>
  <p>オリジナルクリアファイルWebでは、初めてクリアファイルを制作されるお客様にも選んでいただきやすいよう、ステップオーダー方式を採用しております。より詳細の情報が知りたいなど疑問点ありましたら、スタッフまでお気軽にお問い合わせください。</p>

	<h3>名入れオーダーで選べる仕様</h3>
  <table class="fullorder_table">
  <tr class="semi-white">
  <th>形状を選ぶ</th>
  <td>ノーマル</td>
  <td><a href="<?php bloginfo('url'); ?>/shape"><img src="<?php bloginfo('template_url'); ?>/img/fullOrder_btn.png" alt="詳しくはこちら"></a></td>
  </tr>
  <tr class="non-color">
  <th>サイズを選ぶ</th>
  <td>A4/B5/A5/A6</td>
  <td><a href="<?php bloginfo('url'); ?>/size"><img src="<?php bloginfo('template_url'); ?>/img/fullOrder_btn.png" alt="詳しくはこちら"></a></td>
  </tr>
  <tr class="semi-white">
  <th>素材を選ぶ</th>
  <td>ナチュラルPP</td>
  <td><a href="<?php bloginfo('url'); ?>/material"><img src="<?php bloginfo('template_url'); ?>/img/fullOrder_btn.png" alt="詳しくはこちら"></a></td>
  </tr>
  <tr class="non-color">
  <th>印刷方法を選ぶ</th>
  <td>1色印刷/箔押し</td>
  <td><a href="<?php bloginfo('url'); ?>/print"><img src="<?php bloginfo('template_url'); ?>/img/fullOrder_btn.png" alt="詳しくはこちら"></a></td>
  </tr>
  <tr class="semi-white">
  <th>オプションを選ぶ</th>
  <td>OPP袋封入/なし</td>
  <td><a href="<?php bloginfo('url'); ?>/option"><img src="<?php bloginfo('template_url'); ?>/img/fullOrder_btn.png" alt="詳しくはこちら"></a></td>
  </tr>
  </table>
  
  <p class="mt50">
  <a href="<?php echo home_url(); ?>/price#price_name" class="mr10"><img src="<?php bloginfo('template_url'); ?>/img/btn_nameorderPrice.jpg" alt="名入れオーダーの参考価格例を見る"></a>
  <a class="ml5" href="<?php echo home_url(); ?>/estimation_name"><img src="<?php bloginfo('template_url'); ?>/img/btn_nameorderEst.jpg" alt="名入れオーダーのお見積もりはコチラから"></a>
  </p>


<?php get_template_part('part','contact'); ?>				

  
</div><!-- .col-xs-13 -->

<?php get_footer(); ?>