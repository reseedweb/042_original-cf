jQuery( function() {
    jQuery( '#jquery-form input[type=radio],#jquery-form input[type=checkbox]' ) . change(
        function () {
            jQuery( '#jquery-form input' ) . closest( '.check' ) . css( {
                backgroundColor: '',
                borderColor: '',
            } );
            jQuery( '#jquery-form :checked' ) . closest( '.check' ) . css( {
                backgroundColor: '#FFCD36',
                borderRadius: '4px',
            } );
        }
    ) . change();
} );
