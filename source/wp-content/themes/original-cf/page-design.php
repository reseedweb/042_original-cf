<?php
/*
Template Name: design
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">
  
<?php get_template_part('part','bread'); ?>				


  <h2 class="content_h2 mt0"><i class="fa fa-circle-o"></i>オリジナルデザイン制作もお任せください</h2>
  <p>オリジナルクリアファイルWebでは、「デザイン制作は難しい…」「イメージはあるけれど、データを作成できない…」といったお客様のために、ファイルのオリジナルデザイン・ロゴ制作もお引き受けいたします。(別途費用)<br /><br />
  デザイン案は、弊社のデザイナーがお客様が思い描いているイメージやご要望をヒアリングし、ご提案いたします。参考のイメージや画像などがあれば、スタッフまでお伝えください。</p>
  
  <p><img src="<?php bloginfo('template_url'); ?>/img/img_design01.jpg" alt="袋デザイン・ロゴデザインお引き受けします！"></p>


  <h2 class="content_h2"><i class="fa fa-circle-o"></i>デザイン制作料金表</h2>
  <table class="design_table">
  <tr class="semi-white">
  <th class="design"></th>
  <th class="price">価格</th>
  <th>作業内容</th>
  </tr>
  <tr class="non-color">
  <td>クリアファイルデザインA</td>
  <td class="price">30,000 円～</td>
  <td>お客様のご要望をヒアリングしたうえで、オリジナルデザインを作成いたします。</td>
  </tr>
  <tr class="semi-white">
  <td>クリアファイルデザインB</td>
  <td class="price">20,000 円～</td>
  <td>お客様にてイメージ図案がある場合、デザイン起こしいたします。</td>
  </tr>
  <tr class="non-color">
  <td>ロゴ作成A</td>
  <td class="price">30,000 円～</td>
  <td>オリジナル・ロゴ3案作成いたします。</td>
  </tr>
  <tr class="semi-white">
  <td>ロゴ作成B</td>
  <td class="price">10,000 円～</td>
  <td>既存フォントを使用した場合でのロゴ作成となります。</td>
  </tr>
  <tr class="non-color">
  <td>ロゴデータ変換</td>
  <td class="price">5,000 円～</td>
  <td>お持ちの画像データ等から近似値にてデータ変換いたします。</td>
  </tr>
  <tr class="semi-white">
  <td>クリアファイルサンプル作成</td>
  <td class="price">25,000 円～</td>
  <td>クリアファイルをサンプル印刷作成いたします。</td>
  </tr>
  </table>


	
	
<?php get_template_part('part','contact'); ?>				


</div><!-- .col-xs-13 -->

<?php get_footer(); ?>