<?php
/*
Template Name: print
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">
  
<?php get_template_part('part','bread'); ?>				


  <h2 class="content_h2 mt0"><i class="fa fa-circle-o"></i>クリアファイルの印刷方法を選ぶ</h2>
  <p>オリジナルクリアファイルの印刷方法・色数をデザインに合わせて選びます。<br />
  １色印刷から、４色フルカラーまで対応しておりますので、写真やグラデーションのあるデザインも綺麗に印刷可能です。<br />
  会社名やロゴをワンポイントで入れられる、箔押し（ホットスタンプ）も人気です！</p>
  
  <h3>１色印刷</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_print01.jpg" alt="１色印刷" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>企業・店舗・イベントなどのロゴや会社名を１色印刷します。<br />
      基本は透明のクリアファイルに１色でデザインを乗せる印刷になります。<br />
      クリアファイルの全面に印刷も可能です。<br />
      イラストやデザインをシンプルに表現できます。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->
  
  
  <h3>２色印刷</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_print02.jpg" alt="２色印刷" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>オリジナルデザインを透明のクリアファイルに２色で印刷します。<br />
      基本は透明のクリアファイルに２色でデザインを乗せる印刷になります。<br />
      クリアファイルの全面に印刷も可能です。<br />
      よりデザインのバリエーションが広がり、シンプルながらもメッセージ性の高いオリジナルクリアファイルが作成できます。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->
  
  <h3>４色フルカラー</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_print03.jpg" alt="４色フルカラー" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>クリアファイルに４色フルカラーで印刷します。<br />
      基本は透明のクリアファイルにフルカラーでデザインを乗せる印刷になります。<br />
      クリアファイルの全面に印刷も可能です。<br />
      カラフルなイラストやグラデーション、写真を使ったデザインも鮮やかに印刷できます。<br />
      色数の多いキャラクターグッズやノベルティグッズなどにもお勧めです。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->

  <h3>箔押し</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_print04.jpg" alt="箔押し" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>メタリック顔料をホイル加工した金や銀の箔を、クリアファイルの表面に熱転写します。<br />
      輝く箔はロゴマークや企業名を、印象的に魅せ高級感のある仕上がりになります。<br />
      比較的安価に制作できますが、箔押しをする範囲によってお値段が変わります。（範囲が広いほど高額になります）<br />
      箔の色は８種類の中からお好きなものをお選びいただけます。<br /><br />
      
      <a href="">カラーサンプルはこちら</a></p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->


<div class="pager mt50">
<ul>
<li class="prev mr20"><a class="mr10" href="<?php bloginfo('url'); ?>/material"><i class="fa fa-sort-desc fa-rotate-90"></i>STEP3 素材を選ぶ</a></li>
<li class="next mr30"><a class="mr10" href="<?php bloginfo('url'); ?>/option">STEP5 オプションを選ぶ<i class="fa fa-sort-asc fa-rotate-90"></i></a></li>
</ul>
</div>
 
    
<?php get_template_part('part','contact'); ?>				


    </div><!-- .col-xs-13 -->
	</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>