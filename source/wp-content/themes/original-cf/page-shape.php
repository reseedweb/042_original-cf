<?php
/*
Template Name: shape
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">
  
<?php get_template_part('part','bread'); ?>				


  <h2 class="content_h2 mt0"><i class="fa fa-circle-o"></i>クリアファイルの形状を選ぶ</h2>
  <p>オリジナルクリアファイルの形状を、豊富な種類の中から用途に合わせてお選びいただけます。<br />
  一般的なノーマルタイプから便利なポケットや仕切り付きも！<br />
  さらに人気の封筒型やバッグ型ファイルも制作可能です。<br />
  配布後もデスクグッズとして長く使えるから、ノベルティグッズとして宣伝効果も大いに期待できます！</p>
  
  <h3 id="1">ノーマル</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_shape01.jpg" alt="ノーマル" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>一般に広く使われているクリアファイルの形状です。<br />
      クリアファイルの全面にオリジナルデザインが印刷できます。<br />
      使いやすく、どんな用途にも使用できます。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->
  
  
  <h3 id="2">ポケット付き</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_shape02.jpg" alt="ポケット付き" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>バッグの底部分が船の形状をしたタイプで、たっぷり収納したい場合には最適なアイテムです。<br>
      角底タイプと比較して、安い価格にて制作が可能です。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->
  
  <h3 id="3">ロングポケット付き</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_shape03.jpg" alt="ロングポケット付き" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>見開きのファイルの内側に、縦長のポケットが付いています。<br />
      表紙がついているので、品質が高く高級感のあるクリアファイルに仕上がります。<br />
      「ポケット付き」よりもさらに収納力がアップし、使い勝手も良くなります。<br />
      バラバラの書類をいれてもファイルから抜け落ちる可能性が少なく、書類・資料がまとまって楽に持ち運びができます。<br />
      ポケットは見開きの両側にダブルでつけることも可能です。（スタッフまでお問い合わせください）</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->
  
  <h3 id="4">仕切り付き</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_shape04.jpg" alt="仕切り付き" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>内側に仕切りがついていて、書類の分類わけや整理に最適なクリアファイルです。<br />
      仕切り部分にも印刷が可能なので、用途に合わせてインデックスを付けたり、デザインをいれたりと多用途に使用いただけます。<br />
      書類整理のために複数のファイルを持ち歩く必要もなくなり、これ一つでスッキリ持ち運べます。<br />
      仕切りの数は２つ、３つと増やすことも可能です。（スタッフまでお問い合わせください）</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->
    
  <h3 id="5">封筒型ファイル縦</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_shape05.jpg" alt="封筒型ファイル縦" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>ファイルにミシン目がついており、切り離すことで封筒からクリアファイルへと変身します。<br />
      見た目にもインパクトがあり、より販促効果の高いものをお求めの方におすすめです。<br />
      もちろん郵送用の封筒としても使え、離れたお客様へのDMとしても効果抜群です。<br />
      さらに紙の封筒よりも耐久性に優れているので、長くお使いいただけます。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->
    
  <h3 id="6">封筒型ファイル横</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_shape06.jpg" alt="封筒型ファイル横" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>ファイルにミシン目がついており、切り離すことで封筒からクリアファイルへと変身します。<br />
      見た目にもインパクトがあり、より販促効果の高いものをお求めの方におすすめです。<br />
      もちろん郵送用の封筒としても使え、離れたお客様へのDMとしても効果抜群です。<br />
      さらに紙の封筒よりも耐久性に優れているので、長くお使いいただけます。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->
    
  <h3 id="7">バッグ型ファイル縦</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_shape07.jpg" alt="バッグ型ファイル縦" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>ファイルにミシン目がついており、切り離すことでバッグからクリアファイルへと変身します。<br />
      取っ手がついているので持ち運びやすく、イベントや展示会などで配布するのに最適です。<br />
      資料配布の目的だけでなく、持って帰っていただいた後もクリアファイルとして使用できるので、長く販促効果を発揮します。<br />
      より実用的でインパクトの強いオリジナルクリアファイル制作を希望される方にお勧めです。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->

  <h3 id="8">バッグ型ファイル横</h3>
  <div class="standard-box cf">
    <img src="<?php bloginfo('template_url'); ?>/img/img_shape08.jpg" alt="バッグ型ファイル横" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>ファイルにミシン目がついており、切り離すことでバッグからクリアファイルへと変身します。<br />
      取っ手がついているので持ち運びやすく、イベントや展示会などで配布するのに最適です。<br />
      資料配布の目的だけでなく、持って帰っていただいた後もクリアファイルとして使用できるので、長く販促効果を発揮します。<br />
      より実用的でインパクトの強いオリジナルクリアファイル制作を希望される方にお勧めです。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->


<div class="pager mt50">
<ul>
<li class="prev mr20"><a class="mr10" href="<?php bloginfo('url'); ?>"><i class="fa fa-sort-desc fa-rotate-90"></i>トップページ</a></li>
<li class="next mr30"><a class="mr10" href="<?php bloginfo('url'); ?>/size">STEP2 サイズを選ぶ<i class="fa fa-sort-asc fa-rotate-90"></i></a></li>
</ul>
</div>
 
    
<?php get_template_part('part','contact'); ?>				


    </div><!-- .col-xs-13 -->
	</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>