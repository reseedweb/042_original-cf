<?php
/*
Template Name: draft
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">
  
<?php get_template_part('part','bread'); ?>				

	
  <div>
  <h2 class="content_h2 mt0"><i class="fa fa-circle-o"></i>デザインデータ入稿の手順</h2>
  <p>オリジナルクリアファイルWebではお客様自身でのデザインデータの作成・入稿をお願いしております。<br>
  こちらではデータ作成時の注意や入稿の手順をご案内しております。</p>
  <ul class="list-inline guide_order_list mt10">
  <li class="pr0"><a href="#draft_step1"><img src="<?php bloginfo('template_url'); ?>/img/nyuko_guide_01.jpg"  width="149" height="110" alt="入稿のご注意"></a></li>
  <li class="pr0 pl0"><a href="#draft_step2"><img src="<?php bloginfo('template_url'); ?>/img/nyuko_guide_02.jpg" width="149" height="110" alt="ダウンロード"></a></li>
  <li class="pr0 pl0"><a href="#draft_step3"><img src="<?php bloginfo('template_url'); ?>/img/nyuko_guide_03.jpg" width="149" height="110" alt="データ作成"></a></li>
  <li class="pr0 pl0"><a href="#draft_step4"><img src="<?php bloginfo('template_url'); ?>/img/nyuko_guide_04.jpg" width="149" height="110" alt="データ入稿"></a></li>
  <li class="pr0 pl0"><a href="#draft_step4"><img src="<?php bloginfo('template_url'); ?>/img/nyuko_guide_05.jpg"width="144" height="110" alt="データ確認"></a></li>
  </ul>            
  
  <div id="draft_step1">
  <h2 class="content_h2" id="datanyuko_step1"><i class="fa fa-circle-o"></i>【必読】入稿時のご注意</h2>
  <p>ご入稿の際は、必ず「完全データ」をご用意ください。<a href="">※完全データとは？</a><br>
  各テンプレートに記載されているご注意も、必ずご確認のうえ入稿頂けるようにお願いします。</p>
  <ul class="list-group nyuko_list">
  <li class="list-group-item list_bg">IllustratorのバージョンはCCまで対応しております。</li>
  <li class="list-group-item">「4色フルカラー」をご希望の方は、カラーモードは<span class="red">CMYK</span>を選択してください。</li>
  <li class="list-group-item list_bg">特色指定の場合は<span class="red">DIC</span>、もしくは<span class="red">PANTONE</span>にてご指定下さい。</li>
  <li class="list-group-item">解像度は<span class="red">350dpi以上</span>にしてください。解像度が低いと画像が荒くなる可能性があります。</li>
  <li class="list-group-item list_bg">Illustratorに配置した画像は全て<span class="red">「埋め込み」</span>してください。</li>
  <li class="list-group-item">フォントは必ず<span class="red">「文字のアウトライン化」</span>をしてください。</li>
  <li class="list-group-item list_bg">データの作成は、原寸にてお願いします。</li>
  <li class="list-group-item">添付ファイルはできるだけ圧縮して、容量は最大3MBくらいにして頂けるようにお願いします。</li>
  </ul>
  </div>
  
  <div id="draft_step2">
  <h2 class="content_h2" id="datanyuko_step2"><i class="fa fa-circle-o"></i>入稿データ作成</h2>
  <p>印刷デザインをお客様が作成し入稿する場合、<br />
  サイズ別のテンプレートに合わせてデザインをしていただく必要があります。</p>
  
  <p>入稿用のテンプレートはIllustratorとPhotoshopのデータをご用意しております。<br />
  お手持ちのソフトで操作可能なデータをダウンロードし、イメージを作成して頂いた上でご入稿ください。</p>
  
  <p>▼各種テンプレートは以下よりダウンロードください。<br />
  （「-」と表記されているサイズ・形状のテンプレートは随時制作しますので、データ作成の前にスタッフまでご相談ください）</p>
  
  <div class="cf">
  <h3>テンプレート</h3>
  <table class="price-table draft-table">
  <thead>
  <tr>
  <th colspan="2">クリアファイル形状</th>
  <th>A4サイズ</th>
  <th>B5サイズ</th>
  <th>A6サイズ</th>
  <th>オリジナルサイズ</th>
  </tr>
  </thead>
  <tbody>
  <tr>
  <td><img src="<?php bloginfo('template_url'); ?>/img/img_draft01.jpg" alt="ノーマル"></td> 
  <td>ノーマル</td> 
  <td>
  <a href="<?php bloginfo('template_url'); ?>/template/A4_nomal_H310ｍｍ_W220ｍｍ.ai"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlAi.jpg" alt=""></a><br />
  <a href="<?php bloginfo('template_url'); ?>/template/A4_nomal_H310ｍｍ_W220ｍｍ.psd"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlPsd.jpg" class="mt5" alt=""></a></td>
  <td>
  <a href="<?php bloginfo('template_url'); ?>/template/B5_nomal_H271ｍｍ_W193ｍｍ.ai"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlAi.jpg" alt=""></a><br />
  <a href="<?php bloginfo('template_url'); ?>/template/B5_nomal_H271ｍｍ_W193ｍｍ.psd"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlPsd.jpg" class="mt5" alt=""></a></td>
  <td>
  <a href="<?php bloginfo('template_url'); ?>/template/A5_nomal_H220ｍｍ_W153ｍｍ.ai"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlAi.jpg" alt=""></a><br />
  <a href="<?php bloginfo('template_url'); ?>/template/A5_nomal_H220ｍｍ_W153ｍｍ.psd"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlPsd.jpg" class="mt5" alt=""></a>
  </td>
  <td>
  <a href="<?php bloginfo('template_url'); ?>/template/A6_nomal_H158ｍｍ_W110ｍｍ.ai"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlAi.jpg" alt=""></a><br />
  <a href="<?php bloginfo('template_url'); ?>/template/A6_nomal_H158ｍｍ_W110ｍｍ.psd"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlPsd.jpg" class="mt5" alt=""></a>
  </td>
  </tr>
  <tr>
  <td><img src="<?php bloginfo('template_url'); ?>/img/img_draft02.jpg" alt="ポケット付き"></td>
  <td>ポケット付き</td>
  <td>
  <a href="<?php bloginfo('template_url'); ?>/template/A4_SinglePocketFile_H310ｍｍ_W230ｍｍ.ai"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlAi.jpg" alt=""></a><br />
  <a href="<?php bloginfo('template_url'); ?>/template/A4_SinglePocketFile_H310ｍｍ_W230ｍｍ.psd"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlPsd.jpg" class="mt5" alt=""></a>
  </td>
  <td class="center">
  －
  </td>
  <td class="center">
  －
  </td>
  <td class="center">
  －
  </td>
  </tr>
  <tr>
  <td><img src="<?php bloginfo('template_url'); ?>/img/img_draft03.jpg" alt="ロングポケット付き"></td>
  <td>ロングポケット付き</td>
  <td>
  <a href="<?php bloginfo('template_url'); ?>/template/A4_SinglePocketFileLong_H310ｍｍ_W225ｍｍ.ai"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlAi.jpg" alt=""></a><br />
  <a href="<?php bloginfo('template_url'); ?>/template/A4_SinglePocketFileLong_H310ｍｍ_W225ｍｍ.psd"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlPsd.jpg" class="mt5" alt=""></a>
  </td>
  <td class="center">
  －
  </td>
  <td class="center">
  －
  </td>
  <td class="center">
  －
  </td>
  </tr>
  <tr>
  <td><img src="<?php bloginfo('template_url'); ?>/img/img_draft04.jpg" alt="仕切り付き"></td>
  <td>仕切り付き</td>
  <td>
  <a href="<?php bloginfo('template_url'); ?>/template/A4_ShikiriFile1_H310ｍｍ_W220ｍｍ.ai"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlAi.jpg" alt=""></a><br />
  <a href="<?php bloginfo('template_url'); ?>/template/A4_ShikiriFile1_H310ｍｍ_W220ｍｍ.psd"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlPsd.jpg" class="mt5" alt=""></a>
  </td>
  <td class="center">
  －
  </td>
  <td class="center">
  －
  </td>
  <td class="center">
  －
  </td>
  </tr>
  <tr>
  <td><img src="<?php bloginfo('template_url'); ?>/img/img_draft05.jpg" alt="封筒型ファイル縦"></td>
  <td>封筒型ファイル縦</td>
  <td>
  <a href="<?php bloginfo('template_url'); ?>/template/A4_futofile_tate_H310ｍｍ_W240ｍｍ.ai"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlAi.jpg" alt=""></a><br />
  <a href="<?php bloginfo('template_url'); ?>/template/A4_futofile_tate_H310ｍｍ_W240ｍｍ.psd"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlPsd.jpg" class="mt5" alt=""></a>
  </td>
  <td class="center">
  －
  </td>
  <td class="center">
  －
  </td>
  <td class="center">
  －
  </td>
  </tr>
  <tr>
  <td><img src="<?php bloginfo('template_url'); ?>/img/img_draft06.jpg" alt="封筒型ファイル横"></td>
  <td>封筒型ファイル横</td>
  <td>
  <a href="<?php bloginfo('template_url'); ?>/template/A4_futofile_yoko_H220ｍｍ_W325ｍｍ.ai"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlAi.jpg" alt=""></a><br />
  <a href="<?php bloginfo('template_url'); ?>/template/A4_futofile_yoko_H220ｍｍ_W325ｍｍ.psd"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlPsd.jpg" class="mt5" alt=""></a>
  </td>
  <td class="center">
  －
  </td>
  <td class="center">
  －
  </td>
  <td class="center">
  －
  </td>
  </tr>
  <tr>
  <td><img src="<?php bloginfo('template_url'); ?>/img/img_draft07.jpg" alt="バッグ型ファイル縦"></td>
  <td>バッグ型ファイル縦</td>
  <td>
  <a href="<?php bloginfo('template_url'); ?>/template/A4_FileBag_tate_H360ｍｍ_W240ｍｍ.ai"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlAi.jpg" alt=""></a><br />
  <a href="<?php bloginfo('template_url'); ?>/template/A4_FileBag_tate_H360ｍｍ_W240ｍｍ.psd"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlPsd.jpg" class="mt5" alt=""></a>
  </td>
  <td class="center">
  －
  </td>
  <td class="center">
  －
  </td>
  <td class="center">
  －
  </td>
  </tr>
  <tr>
  <td><img src="<?php bloginfo('template_url'); ?>/img/img_draft08.jpg" alt="バッグ型ファイル横"></td>
  <td>バッグ型ファイル横</td>
  <td>
  <a href="<?php bloginfo('template_url'); ?>/template/A4_FileBag_yoko_H290ｍｍ_W380ｍｍ.ai"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlAi.jpg" alt=""></a><br />
  <a href="<?php bloginfo('template_url'); ?>/template/A4_FileBag_yoko_H290ｍｍ_W380ｍｍ.psd"><img src="<?php bloginfo('template_url'); ?>/img/btn_dlPsd.jpg" class="mt5" alt=""></a>
  </td>
  <td class="center">
  －
  </td>
  <td class="center">
  －
  </td>
  <td class="center">
  －
  </td>
  </tr>
  </tbody>
  </table>
  </div>
  </div>
  
  <div id="draft_step3">
  <h2 class="content_h2"><i class="fa fa-circle-o"></i>デザインデータ作成</h2>
  <p>デザインの原稿を、イラストレーター(ai/eps)データで作成ください。<br>デザインデータ作成の前には、下記と入稿用テンプレートに記載されたご注意を必ずご覧ください。<br>「デザインは苦手」「イメージはあるけどデータに起こせない」というお客様は、弊社のデザイナーがデザインの制作をさせて頂くことも可能です。(別途デザイン費が必要です)お気軽にご相談下さい。</p>
  <!--<p><a href="http://original-db.net/dsign"><img src="<?php bloginfo('template_url'); ?>/img/content_design.jpg" height="213" width="760"></a></p>-->
  </div>
  
  <div id="draft_step4">
  <h2 class="content_h2"><i class="fa fa-circle-o"></i>データ入稿はこちらから</h2>
  <p>デザインデータができましたら下記「データ入稿する」ボタンから、メールにて添付でお送りください。</p>
  <p><a href="mailto:data@rebecca-g.com"><img src="<?php bloginfo('template_url'); ?>/img/nyuko_btn.jpg" height="45" width="300" alt="データ入稿する"></a></p>
  </div>
  
  <div id="draft_step5">
  <h2 class="content_h2"><i class="fa fa-circle-o"></i>「完全データ」での入稿をお願いしております</h2>
  <p>「完全データ」とは、修正の必要がない完成された印刷可能なデザインデータのことです。<br />
データ入稿の前には、いまいちど「サイズ・色数に間違いがないか」「入稿の注意に沿っているデータか」入念なチェックをお願いいたします。</p>
	<p>デザイン入稿後、弊社のスタッフにより入稿データの確認を行います。<br />
完全データではなかった場合、お客様にて修正いただき、再入稿をお願いしております。<br />
再入稿後、「完全データ」の確認がとれた時点を「入稿日」とさせていただきます。<br />
ご入稿日が変わってきますと出荷予定日も同様に変わりますので、当初のご入稿日から計算される納品日が異なってまいります。あらかじめご了承ください。</p>

	<p>入稿についてご不明点がありましたら、弊社スタッフまでお気軽にお問い合わせください。</p>
  </div>
 
	
<?php get_template_part('part','contact'); ?>				
        

    </div><!-- .col-xs-13 -->
</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>