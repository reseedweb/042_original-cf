<?php
/*
Template Name: design
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">
  
<?php get_template_part('part','bread'); ?>				

	<h2 class="price_normal-h2 mt0"><i class="fa fa-circle-o"></i>オリジナルデザイン制作もお任せくだ さい</h2>
	<p>オリジナルクリアファイルの制作にどれぐらいの価格がかかるんだろう？という疑問にお応えするため、わかりやすい参考価格例を掲載しています。（※あくまで参考の価格になります。）実際のお見積もり価格はクリアファイルの仕様により変動します。詳しくはお電話か見積もりフォームにてお問い合わせください。</p>
	<ul class="price_normal-navi clearfix">
	    <li>
			<a class="on_off" href="<?php bloginfo('url');?>/#"> 
				<img src="<?php bloginfo('template_url'); ?>/img/price_normal_btn1Off.jpg" data-hover="<?php bloginfo('template_url'); ?>/img/price_normal_btn1On.jpg"/>
			</a>
		</li>
	    <li>
			<a class="on_off" href="<?php bloginfo('url');?>/#"> 
				<img src="<?php bloginfo('template_url'); ?>/img/price_normal_btn2Off.jpg" data-hover="<?php bloginfo('template_url'); ?>/img/price_normal_btn2On.jpg"/>
			</a>
		</li>
		<li>
			<a class="on_off" href="<?php bloginfo('url');?>/#"> 
				<img src="<?php bloginfo('template_url'); ?>/img/price_normal_btn3Off.jpg" data-hover="<?php bloginfo('template_url'); ?>/img/price_normal_btn3On.jpg"/>
			</a>
		</li>
		<li>
			<a class="on_off" href="<?php bloginfo('url');?>/#"> 
				<img src="<?php bloginfo('template_url'); ?>/img/price_normal_btn4Off.jpg" data-hover="<?php bloginfo('template_url'); ?>/img/price_normal_btn4On.jpg"/>
			</a>
		</li>
		<li>
			<a class="on_off" href="<?php bloginfo('url');?>/#"> 
				<img src="<?php bloginfo('template_url'); ?>/img/price_normal_btn5Off.jpg" data-hover="<?php bloginfo('template_url'); ?>/img/price_normal_btn5On.jpg"/>
			</a>
		</li>
	</ul><!-- end price_normal-navi -->
	<h3>ノーマルタイプの特長</h3>
	<div class="price_normal-content clearfix">
		<div class="price_normal-img">
			<p><img src="<?php bloginfo('template_url'); ?>/img/price_normal_img1.jpg" alt="<?php bloginfo('name'); ?>" /></p>
			<p><a href="<?php bloginfo('url'); ?>/#est_normal"><img src="<?php bloginfo('template_url'); ?>/img/price_normal_btn.jpg" alt="<?php bloginfo('name'); ?>" /></a></p>
		</div>
		<div class="price_normal-text">
			<h4 class="price_normal-title">特長</h4>
			<p>一般に広く使われているクリアファイルの形状です。<br />クリアファイルの全面にオリジナルデザインが印刷できます。<br />使いやすく、どんな用途にも使用できます。</p>
			<p class="pt15"><img src="<?php bloginfo('template_url'); ?>/img/price_normal_img2.jpg" alt="<?php bloginfo('name'); ?>" /></p>
		</div>
	</div><!-- end price_normal-content --->
	
	<h3 class="mt35">ノーマルタイプの参考価格例</h3>
	<p class="pt5">※下記のサイズ・枚数でご注文した際の１枚あたりの単価を表記しています。<br />※価格は全て税抜き表示となっております。<br />素材：PPナチュラル 0.2mm厚 / オプション：なし</p>
	<h4 class="price_normal-h4">４色フルカラー印刷クリアファイル</h4>
	<table class="price_normal-table">
		<thead>
			<tr>
				<th></th>
				<th colspan="2">300枚</th>				
				<th colspan="2">500枚</th>
				<th colspan="2">1,000枚</th>
				<th colspan="2">3,000枚</th>
				<th colspan="2">5,000枚</th>
				<th colspan="2">10,000枚</th>
			</tr>
			<tr>
				<th></th>
				<th>総額</th>
				<th>単価</th>
				<th>総額</th>
				<th>単価</th>
				<th>総額</th>
				<th>単価</th>
				<th>総額</th>
				<th>単価</th>
				<th>総額</th>
				<th>単価</th>
				<th>総額</th>
				<th>単価</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>A4サイズ</td> 
				<td>¥36,900</td>
				<td>¥123</td>
				<td>¥42,000</td>
				<td>¥84</td>
				<td>¥54,000</td>
				<td>¥54</td>
				<td>¥99,000</td>
				<td>¥33</td>
				<td>¥142,500</td>
				<td>¥29</td>
				<td>¥255,000</td>
				<td>¥26</td>
 			<tr>
			<tr>
				<td>B5サイズ</td> 
				<td>¥49,980</td>
				<td>¥167</td>
				<td>¥55,000</td>
				<td>¥110</td>
				<td>¥67,000</td>
				<td>¥67</td>
				<td>¥111,900</td>
				<td>¥37</td>
				<td>¥158,000</td>
				<td>¥32</td>
				<td>¥272,000</td>
				<td>¥27</td>
 			<tr>
			<tr>
				<td>A5サイズ</td> 
				<td>¥49,980</td>
				<td>¥167</td>
				<td>¥50,000</td>
				<td>¥100</td>
				<td>¥56,000</td>
				<td>¥56</td>
				<td>¥84,000</td>
				<td>¥28</td>
				<td>¥112,000</td>
				<td>¥22</td>
				<td>¥182,000</td>
				<td>¥18</td>
 			<tr>
			<tr>
				<td>A6サイズ</td> 
				<td>¥46,800</td>
				<td>¥156</td>
				<td>¥49,000</td>
				<td>¥98</td>
				<td>¥56,000</td>
				<td>¥56</td>
				<td>¥82,800</td>
				<td>¥28</td>
				<td>¥110,000</td>
				<td>¥22</td>
				<td>¥178,000</td>
				<td>¥18</td>
 			<tr>
		</tbody>
	</table><!-- end price_normal-table -->
	
	<h3 class="mt35">ノーマルタイプの参考価格例</h3>
	<p class="pt10">オリジナルクリアファイル紙袋のお見積もりフォームです。<br />必要箇所をご記入頂き「送信する」ボタンをクリックしてください。<br />専門スタッフがメールを確認後、即日対応させて頂きます。<br />（営業時間終了後のご送信の場合、ご返信は翌営業日になります。ご了承ください）</p>
<?php get_template_part('part','contact'); ?>				
</div><!-- .col-xs-13 -->

<?php get_footer(); ?>