<?php
/*
Template Name: option
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">
  
<?php get_template_part('part','bread'); ?>				


  <h2 class="content_h2 mt0"><i class="fa fa-circle-o"></i>クリアファイルのオプションを選ぶ</h2>
  <p>オリジナルクリアファイルに様々な表面加工やオプションをつけることができます。<br />
  そうすることで他にはないインパクトをプラスしPR効果もさらにUP！<br />
  どんな加工か想像が難しい場合は、無料サンプルが送付できる場合もあります。(在庫により送付できない場合もあります。ご了承ください。)<br />
  お電話やメールでも詳しいご説明も可能です。<br />
  なにか気になるオプションがあれば、お気軽にスタッフまでお問い合わせください。</p>
  
  <h3>エンボス加工</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_option01.jpg" alt="エンボス加工" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>クリアファイルの表面に様々な模様を押し当てて、柄をつけます。<br />
      模様に沿って表面に凹凸ができるので、手触りも変化し他にはないオリジナルクリアファイルに仕上がります。<br />
      柄によって高級感を引き立てることも可能です。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->
  
  
  <h3>ホログラム加工</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_option02.jpg" alt="ホログラム加工" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>クリアファイルの表面にトランスタバック加工（ホログラム加工）を施します。<br />
      どの角度からみてもキラキラと輝いたホログラムが目を引き、インパクトは抜群です。<br />
      オリジナルクリアファイルのPR効果を高めたい方にお勧めです。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->
  
  <h3>圧着加工</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_option03.jpg" alt="圧着加工" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>クリアファイルの内側に銀色のフィルムを圧着加工いたします。<br />
      印刷を入れていない部分から銀色が透けて見えます。<br />
      デザインによって非常にユニークなオリジナルクリアファイルに仕上げることができます。<br />
      他にはないクリアファイルの制作をお求め方はぜひお試しください。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->

  <h3>名刺ポケット付き</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_option04.jpg" alt="名刺ポケット付き" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>クリアファイルの一部に、名刺を挟み込むことのできるポケットをお付けします。<br />
      配布時に名刺やご案内を挿入して配れば、宣伝PR効果も高まり、名刺を紛失することもありません。<br />
      様々な用途に使え非常に利便性の高いクリアファイルです。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->

<h3>OPP袋封入</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_option05.jpg" alt="OPP袋封入" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>印刷済みのオリジナルクリアファイルを、１枚１枚OPPの袋に入れて包装納品します。
販売・グッズ用のクリアファイルや、見た目の品質をアップされされたい方に重宝されています。
イベント・ノベルティグッズなどにお勧めです。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->


<div class="pager mt50">
<ul>
<li class="prev mr20"><a class="mr10" href="<?php bloginfo('url'); ?>/print"><i class="fa fa-sort-desc fa-rotate-90"></i>STEP4 印刷方法を選ぶ</a></li>
<li class="next mr30"><a class="mr10" href="<?php bloginfo('url'); ?>/price">STEP6 参考価格例<i class="fa fa-sort-asc fa-rotate-90"></i></a></li>
</ul>
</div>
 
    
<?php get_template_part('part','contact'); ?>				


    </div><!-- .col-xs-13 -->
	</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>