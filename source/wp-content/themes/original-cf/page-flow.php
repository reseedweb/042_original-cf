<?php
/*
Template Name: flow
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">
  
<?php get_template_part('part','bread'); ?>				


  <h2 class="content_h2 mt0"><i class="fa fa-circle-o"></i>
  お問い合わせから納品までの流れ</h2>
  <p>オリジナルクリアファイルWebに問い合わせしていただいてから、<br />
  商品が製造・納品されるまでの流れをご案内いたします。</p>

 
  <h3 class="h3_flow01">1. サンプル請求・お問い合わせ</h3>
  <div class="cf">
  <img src="<?php bloginfo('template_url'); ?>/img/img_flow01.jpg" alt="サンプル請求・お問い合わせ" class="pull-left mr20">
  <p class="flow_txt pull-right">まずは当サイトのオーダーガイド(STEP1〜5)を参考に、<br />
  希望のオリジナルクリアファイルの仕様をご検討ください。<br />
  「デザインや形状・予算から相談したい」「どんな素材、サイズ、加工を選べば良いかわからない」というお客様もお気軽にお問い合わせください。些細な疑問も丁寧にお応えさせていただきます。<br />
  ご希望があれば、クリアファイルのサンプルをお送りすることも可能です。(在庫により送付できない場合もございます。ご了承ください)<br /><br />
  
  お問い合わせはお電話(0120-838-117)か、当サイトの"お問い合わせフォーム"をご利用ください。<br />
  その際、スタッフまで用途・サイズ・納期・ロットなどのご希望をお伝えいただけるとスムーズです。</p>
  </div> 
  <p class="text-center"><img src="<?php bloginfo('template_url'); ?>/img/img_arrow.jpg" alt="▼"></p>
  
  <h3 class="h3_flow02">2. お見積もり</h3>
  <div class="cf">
  <img src="<?php bloginfo('template_url'); ?>/img/img_flow02.jpg" alt="お見積もり" class="pull-left mr20">
  <p class="flow_txt pull-right">お伺いしたオリジナルクリアファイルの仕様をもとに、お見積もりをお出しいたします。<br />
  枚数やサイズ違いなど、複数パターンのお見積もりも同時にお出しすることが可能です。<br />
  お気軽にお申し付けください。<br />
  もし予算がお決まりでしたら、ぜひ見積もりスタッフにご予算をお聞かせください。<br />
  予算内に納めるお客様のご要望に合わせたオリジナルプランをご提案いたします。</p>
  </div> 
  <p class="text-center"><img src="<?php bloginfo('template_url'); ?>/img/img_arrow.jpg" alt="▼"></p>
  
  <h3 class="h3_flow03">3. ご発注・データ入稿</h3>
  <div class="cf">
  <img src="<?php bloginfo('template_url'); ?>/img/img_flow03.jpg" alt="ご発注・データ入稿" class="pull-left mr20">
  <p class="flow_txt pull-right">オリジナルクリアファイルの仕様とお見積もり金額に納得いただきましたら、正式にご発注となります。<br />
  発注やお支払いについてはスタッフが適宜ご案内いたします。<br />
  基本的には全額前払い制をとらせていただいております。(難しい場合は事前にご相談ください)<br /><br />
  ご発注がお済みになりましたら、デザインデータの入稿をお願いいたします。<br />
  弊社では「完全データ」での入稿をお願いしております。もし完全データでない場合には、再入稿の必要がありますのでご注意ください。<br />
  入稿について詳しくは「データ入稿について」のページをご覧ください。<br />
ご自身でデザインデータを作ることが難しい場合は、弊社でデータ作成およびデザイン制作を承ることも可能です。(別途費用)</p>
  </div> 
  <p class="text-center"><img src="<?php bloginfo('template_url'); ?>/img/img_arrow.jpg" alt="▼"></p>
  
  <h3 class="h3_flow04">4. 工場にて制作・発送</h3>
  <div class="cf">
  <img src="<?php bloginfo('template_url'); ?>/img/img_flow04.jpg" alt="工場にて制作・発送" class="pull-left mr20">
  <p class="flow_txt pull-right">入稿いただきましたデザインデータをもとに、海外もしくは国内の提携工場にしてオリジナルクリアファイルの制作を行います。<br />
  制作されたオリジナルクリアファイルは、丁寧に検品をされ、工場より発送されます。<br />
  海外からの輸送の場合は、国内生産に比べ制作から納品までお時間をいただく場合がございます。<br />
  基本的にはデータ入稿から３日〜１週間で制作が可能です。</p>
  </div> 
  <p class="text-center"><img src="<?php bloginfo('template_url'); ?>/img/img_arrow.jpg" alt="▼"></p>
  
  <h3 class="h3_flow05">5. ご納品</h3>
  <div class="cf mb30">
  <img src="<?php bloginfo('template_url'); ?>/img/img_flow05.jpg" alt="ご納品" class="pull-left mr20">
  <p class="flow_txt pull-right">仕上がったオリジナルクリアファイルを、ご指定の日時・場所に納品いたします。<br />
  ぐに納品物を開け、完成品の状態をお確かめください。万が一状態に不備のある場合は、お手数ですがご連絡をお願いいたします。<br />
  クリアファイルを複数箇所に分納することもできます。ご希望の場合はスタッフまでお気軽にお申し付けください。</p>
  </div> 
  
  
<?php get_template_part('part','contact'); ?>				

  
</div><!-- .col-xs-13 -->

<?php get_footer(); ?>