<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">
  
<?php get_template_part('part','bread'); ?>				


<?php if (have_posts()) : query_posts($query_string . '&posts_per_page=1'); ?>
	<?php while(have_posts()): the_post(); ?>

  <h2 class="content_h2 mt0"><i class="fa fa-circle-o"></i><?php the_title(); ?></h2>

  <div class="clearfix">
  
  <div class="jisseki-img pull-left mt20">
	<?php echo wp_get_attachment_image(get_post_meta($post->ID,"Image",true),'full'); ?>
  </div>
  
  <div class="jisseki-table pull-right">
  <h3><?php echo post_custom("Copy")?></h3>
  <table class="price-table">
  <tr>
  <th>形状</th>
  <td><?php echo post_custom("Shape")?></td>
  </tr>
  <tr>
  <th>サイズ</th>
  <td><?php echo post_custom("Size")?></td>
  </tr>
  <tr>
  <th>素材</th>
  <td><?php echo post_custom("Material")?></td>
  </tr>
  <tr>
  <th>印刷方法</th>
  <td><?php echo post_custom("Print")?></td>
  </tr>
  </table>
  </div>
  
  </div><!-- /clearfix -->
		

  <div class="blog-text mt10 mb40">
  <?php the_content(); ?>
  
  <p class="taxonomy-link mt20">
  日付：<?php echo date("Y年m月d日", strtotime($post->post_date)); ?>
  　　カテゴリ：
  <?php $cat = get_the_category();
  $pid = $cat[0]->parent;
  echo '<a href=' . get_category_link( $cat[0]->term_id ) . '>' . $cat[0]->name . '</a>'; ?>
  <?php the_tags('　活用例：', ' , '); ?></p>
  </div>
 
 
  <!-- post navigation -->
  <div class="pager mt40">
	<?php previous_post('&laquo; %','前のページ','no') ?> ｜	<?php next_post('% &raquo;','次のページ','no') ?>
  </div>
  <!-- /post navigation -->

	<?php endwhile; ?>
<?php wp_reset_query();endif; ?>
<?php wp_reset_query(); ?>     


<?php get_template_part('part','contact'); ?>

<?php get_footer(); ?>

