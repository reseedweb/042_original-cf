<?php
/*
Template Name: faq
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">
  
<?php get_template_part('part','bread'); ?>				

 
  <div class="faq">
  
  <h2 class="content_h2 mt0"><i class="fa fa-circle-o"></i>よくあるご質問</h2>
  <p>お客様からお問い合わせの多いご質問について、回答を掲載しております。<br />
  下記以外の疑問・お悩みにつきましてはお気軽にお問い合わせください。</p>

  <div class="cf row">
  <div class="col-xs-9">
  <h3>クリアファイルの制作について</h3>
  <ul class="list-unstyled ml10">
  <li><a href="#q1">Q1. サンプルはもらえますか？</a></li>
  <li><a href="#q2">Q2. 素材の色は選べますか？</a></li>
  <li><a href="#q3">Q3. 1枚ずつOPP袋に入れてもらうことはできますか？</a></li>
  <li><a href="#q4">Q4. 素材を支給して、印刷・加工をしてもらうことは可能ですか？</a></li>
	</ul>
  <h3>デザインデータ入稿について</h3>
  <ul class="list-unstyled ml10">
  <li><a href="#q5">Q5. 印刷物を入れていたらファイルがめくれてしまいました。不良でしょうか？</a></li>
  <li><a href="#q6">Q6. データ入稿の方法を教えてください。</a></li>
  <li><a href="#q7">Q7. 完全データとはなんでしょうか？</a></li>
  <li><a href="#q8">Q8. 白版が必要なのはどんな時ですか？</a></li>
  <li><a href="#q9">Q9. デザインやデータの作成が難しいのですが…</a></li>
  <li><a href="#q10">Q10. 印刷できないデザインはありますか？</a></li>
  </ul>
  </div>
  <div class="col-xs-9">
<!--  <ul class="list-unstyled ml10">
  <li><a href="#q8">Q8. 白版が必要なのはどんな時ですか？</a></li>
  <li><a href="#q9">Q9. デザインやデータの作成が難しいのですが…</a></li>
  <li><a href="#q10">Q10. 印刷できないデザインはありますか？</a></li>
  </ul>-->
  <h3>ご注文について</h3>
  <ul class="list-unstyled ml10">
  <li><a href="#q11">Q11. 数回にわけて納品してもらうことは可能ですか？</a></li>
  <li><a href="#q12">Q12. 複数箇所へ納品してもらうことは可能ですか？</a></li>
  <li><a href="#q13">Q13. 支払いのタイミングと振込先を教えてください</a></li>
  <li><a href="#q14">Q14. 発注のキャンセルは可能ですか？</a></li>
  </ul>
  </div>
  </div><!-- /row -->
  
  <h2 class="content_h2"><i class="fa fa-circle-o"></i>クリアファイルの制作について</h2>
  <div class="panel panel-info" id="q1">
  <div class="panel-heading">Q1. 作成を検討したいのですが、クリアファイルのサンプルはもらえますか？</div>
  <div class="panel-body">
  <p>可能です。どのようなサンプルが欲しいかをスタッフまでお申し付けください。<br />
  在庫の関係で送付できない場合もございます。ご了承ください。</p>
  </div>
  </div>
  
  <div class="panel panel-info" id="q2">
  <div class="panel-heading">Q2. クリアファイル自体の素材の色は選べますか？</div>
  <div class="panel-body">
  <p>基本的には透明のクリアファイルに印刷をいたします。<br />
  クリアファイル自体に色をつけることも可能ですが、ある程度のロット数が必要になります。<br />
  ご希望の方はスタッフまでご相談ください。</p>
  </div>
  </div>
  
  <div class="panel panel-info" id="q3">
  <div class="panel-heading">Q3. クリアファイルを1枚ずつOPP袋に入れてもらうことはできますか？</div>
  <div class="panel-body">
  <p>可能です。オプションで「OPP袋封入」をお選びください。<br />
クリアファイル１枚1枚をそれぞれOPP袋で包装して納品いたします。</p>
  </div>
  </div>
  
  <div class="panel panel-info" id="q4">
  <div class="panel-heading">Q4. 素材(クリアファイル)を支給して、印刷・加工をしてもらうことは可能ですか？</div>
  <div class="panel-body">
  <p>申し訳ございませんが、機械との相性・や品質の問題などトラブルの原因になりうるため、お断りしております。</p>
  </div>
  </div>
  
  <div class="panel panel-info" id="q5">
  <div class="panel-heading">Q5. クリアファイルに印刷物を入れていたらファイルがめくれてしまいました。不良でしょうか？</div>
  <div class="panel-body">
  <p>クリアファイル自体の問題ではなく、一般の印刷物からでるガスの影響でクリアファイルがめくれてしまうことがあります。油性のインクで印刷後間もない（完全に乾いていない）印刷物が乾燥する段階で、揮発性のガスを放出し、それがクリアファイルの素材であるポリプロピレンを曲げさせる作用があります。<br />
気になる方は、よく乾いた印刷物を入れることをお勧めいたします。</p>
  </div>
  </div>
  

  <h2 class="content_h2"><i class="fa fa-circle-o"></i>デザインデータ入稿について</h2>

  <div class="panel panel-info" id="q6">
  <div class="panel-heading">Q6. データ入稿の方法を教えてください。</div>
  <div class="panel-body">
  <p>「データ入稿について」のページに記載されている注意書きをよく読み、デザインデータを作成ください。<br />
  入稿用のテンプレートは上記ページからサイズ別のものをダウンロードできます。<br />
  形状によってはサイトからダウンロードできないものもございます。その場合は随時テンプレートをお作りいたしますので、スタッフまでお問い合わせください。<br />
  入稿用のデータが完成次第、下記のアドレスまで添付の上ご入稿ください。<br />
  <a href="mailto:data@rereca.com">data@rereca.com</a><br />
  入稿について詳しくは「データ入稿について」のページをご覧ください。</p>
  </div>
  </div>
  
  <div class="panel panel-info" id="q7">
  <div class="panel-heading">Q7. 完全データとはなんでしょうか？</div>
  <div class="panel-body">
  <p>「完全データ」とは、修正の必要がない完成された印刷可能なデザインデータのことです。<br />
  データ入稿の前には、いまいちど「サイズ・色数に間違いがないか」「入稿の注意に沿っているデータか」入念なチェックをお願いいたします。<br />
  完全データではなかった場合、お客様にて修正いただき、再入稿をお願いしております。<br />
  再入稿後、「完全データ」の確認がとれた時点を「入稿日」とさせていただきます。<br />
  ご入稿日が変わってきますと出荷予定日も同様に変わりますので、当初のご入稿日から計算される納品日が異なってまいります。あらかじめご了承ください。</p>
  </div>
  </div>
  
  <div class="panel panel-info" id="q8">
  <div class="panel-heading">Q8. 白版が必要なのはどんな時ですか？</div>
  <div class="panel-body">
  <p>基本的に透明のクリアファイルにカラー印刷（透明インキ）するため、印刷をしても中身が印刷を通して透けて見える、半透明の仕上がりになります。<br />
  白印刷は不透明のインキになるので、下記３点の場合は白版が必要になります。</p>
  
  <p>・カラーの写真やイラストをいれる場合（写真・絵柄の白い部分は白色インキを使用します）<br />
  ・白い図形やベタの印刷表現をしたい場合<br />
  ・カラー印刷の背景を不透明に（透けにくく）したい場合</p>
  
  <p>白版について詳しくは「データ入稿について」のページをご覧ください。</p>
  </div>
  </div>

  <div class="panel panel-info" id="q9">
  <div class="panel-heading">Q9. デザインやデータの作成が難しいのですが…</div>
  <div class="panel-body">
  <p>お客様ご自身でオリジナルデザインや入稿用データの作成が難しい場合は、弊社のデザイナーが代わりにデザインをお作りすることも可能です。（別途費用）<br />
  クリアファイルのデザインだけでなく、企業や店舗様のロゴデザインも承っております。<br />
  デザイン制作料金については、「オリジナルデザイン制作」のページをご覧ください。</p>
  </div>
  </div>

  <div class="panel panel-info" id="q10">
  <div class="panel-heading">Q10. 印刷できないデザインはありますか？</div>
  <div class="panel-body">
  <p>弊社では以下に該当するもの、又は弊社にて適切でないと判断した内容に関しては制作をお断りさせていただきます。<br />
  ・著作権・肖像権・商標権等の問題が起こる可能性のあるデザイン（権利元から正式な許可がない場合）<br />
  ・ アダルト関連、公序良俗に反する内容のもの<br />
  ・ 法令に違反するもの</p>
  </div>
  </div>


  <h2 class="content_h2"><i class="fa fa-circle-o"></i>ご注文について</h2>

  <div class="panel panel-info" id="q11">
  <div class="panel-heading">Q11. 数回にわけて納品してもらうことは可能ですか？</div>
  <div class="panel-body">
  <p>可能です。なお、その都度送料が必要になります。<br />
  ※保管期間が長い場合は、別途保管料金が必要となって参りますのでご注意下さい。（要相談）</p>
  </div>
  </div>

  <div class="panel panel-info" id="q12">
  <div class="panel-heading">Q12. 複数箇所へ納品してもらうことは可能ですか？</div>
  <div class="panel-body">
  <p>可能です。</p>
  </div>
  </div>

  <div class="panel panel-info" id="q13">
  <div class="panel-heading">Q13. 支払いのタイミングと振込先を教えてください</div>
  <div class="panel-body">
  <p>基本的に全額前払い銀行振込になります。<br />
  もし全額前払いが難しい場合は、ご注文時に担当スタッフまでご相談ください。</p>
  
  <p>お振込先は下記の通りです。<br /><br />
  
  金融機関名：楽天銀行<br />
  支店名：第一営業支店（支店番号：251）<br />
  口座番号：普通口座 7165770<br />
  口座名義：株式会社RESEED</p>
  </div>
  </div>

  <div class="panel panel-info" id="q14">
  <div class="panel-heading">Q14. 発注のキャンセルは可能ですか？</div>
  <div class="panel-body">
  <p>可能ですが、ご注文後キャンセルを希望される場合は、それまでにかかった制作料金に応じてキャンセル料を頂いております。ご了承願います。</p>
  </div>
  </div>

  </div><!-- .row -->	
  
  
<?php get_template_part('part','contact'); ?>				


</div><!-- .col-xs-13 -->

<?php get_footer(); ?>