<?php


?>
<?php

function replaceImagePath($arg) {
$content = str_replace('"img/', '"' . get_bloginfo('template_directory') . '/img/', $arg);
return $content;
}
add_action('the_content', 'replaceImagePath');

?>
<?php add_theme_support( 'post-thumbnails' ); ?>
<?php 
function theme_name_wp_title( $title, $sep ) {
	if ( is_feed() ) {
		return $title;
	}
	
	global $page, $paged;

	// Add the blog name
	$title .= get_bloginfo( 'name', 'display' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title .= " $sep $site_description";
	}

	// Add a page number if necessary:
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
		$title .= " $sep " . sprintf( __( 'Page %s', '_s' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'theme_name_wp_title', 10, 2 );

remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head','adjacent_posts_rel_link_wp_head', 10);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head','wp_shortlink_wp_head',10, 0 );

// 管理画面にjsを追加
function _register_custom_js( ) {
	$_current_theme_dir = get_template_directory_uri();
	$_custom_js = '<script src="/original-cf.net/wp/wp-content/themes/clearfileweb/js/field.js"></script>';
	echo $_custom_js . "\n";
}
add_action('admin_head', '_register_custom_js');

//管理画面カスタム
function edit_admin_menus() {  
	global $menu;
	global $submenu;
	$menu[5][0] = '制作実例'; // '投稿'を'その他'に変更
	$submenu['edit.php'][5][0] = '制作実例一覧';//'投稿一覧'を'その他一覧'へ変更;
}  
add_action( 'admin_menu', 'edit_admin_menus' ); 

function my_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a { display: none; }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

?>
