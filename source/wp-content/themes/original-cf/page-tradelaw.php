<?php
/*
Template Name: tradelaw
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">
  
<?php get_template_part('part','bread'); ?>				


  <h2 class="content_h2 mt0"><i class="fa fa-circle-o"></i>特定商取引法に基づく記載</h2>

  <table class="tradelaw_table">
  <tr class="semi-white">
  <th>販売業社</th>
  <td>株式会社RESEED</td>
  </tr>
  <tr class="non-color">
  <th>代表責任者</th>
  <td>柏木 綾乃</td>
  </tr>
  <tr class="semi-white">
  <th>代表者</th>
  <td>大江 栄年</td>
  </tr>
  <tr class="non-color">
  <th>所在地</th>
  <td>〒532-0011<br />
  大阪市淀川区西中島5丁目6-9 新大阪第一ビル3階(本社営業拠点)<br /><br />
  〒556-0001<br />
  大阪市浪速区下寺2-3-3</td>
  </tr>
  <tr class="semi-white">
  <th>電話番号</th>
  <td>0120-838-117</td>
  </tr>
  <tr class="non-color">
  <th>FAX</th>
  <td>06-4862-4174</td>
  </tr>
  <tr class="semi-white">
  <th>ご注文方法</th>
  <td>お電話・Fax・お問い合わせ・お見積りフォーム</td>
  </tr>
  <tr class="non-color">
  <th>商品代金以外の必要な料金</th>
  <td>商品代金のほか、送料、振込み手数料</td>
  </tr>
  <tr class="semi-white">
  <th>お支払い方法</th>
  <td>銀行振り込み</td>
  </tr>
  <tr class="non-color">
  <th>お振込み先</th>
  <td>
  金融機関名：楽天銀行<br>
  支店名：第一営業支店（支店番号：251）<br>
  口座番号：普通口座 7165770<br>
  口座名義：株式会社RESEED
  </td>
  </tr>
  <tr class="semi-white">
  <th>返品について</th>
  <td>商品の性質上、返品はお受けいたしかねますので、ご了承願います。</td>
  </tr>
  <tr class="non-color">
  <th>キャンセルについて</th>
  <td>ご注文後、キャンセルを希望される場合はキャンセル料金を頂いております。</td>
  </tr>
  <tr class="semi-white">
  <th>個人情報保護</th>
  <td>当サイトのプライバシーポリシーをお読み下さい。</td>
  </tr>
  <tr class="non-color">
  <th>責任の有無</th>
  <td>当サービスの利用、または利用が出来ないことによっていかなるトラブルが発生しても、弊社では一切の責任を負いません。ご了承願います。</td>
  </tr>
  </table>



<?php get_template_part('part','contact'); ?>				

  
</div><!-- .col-xs-13 -->

<?php get_footer(); ?>