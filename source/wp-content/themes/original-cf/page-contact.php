<?php
/*
Template Name: contact
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">

<?php get_template_part('part','bread'); ?>

  <h2 class="content_h2 mt0"><i class="fa fa-circle-o"></i>クリアファイルのお問い合わせ、ご相談はこちら</h2>
  <p>オリジナルクリアファイルWEBのお問い合わせフォームです。<br>
  必要箇所をご記入頂き「送信する」ボタンをクリックしてください。<br>
  専門スタッフがメールを確認後、即日対応させて頂きます。<br>
  （営業時間終了後のご送信の場合、ご返信は翌営業日になります。ご了承ください。）</p>
  

  <div class="estimate_table mb30">
  <?php echo do_shortcode('[contact-form-7 id="8" title="お問い合わせ" html_id="jquery-form" html_class="form-group"]') ?>
  </div>

  <script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
  <script type="text/javascript">
  $(document).ready(function(){
  $('#zip').change(function(){					
  //AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
  AjaxZip3.zip2addr(this,'','pref','addr01','addr02');
  });
  });
  </script>	
  <div>
  <h2 class="content_h2"><i class="fa fa-circle-o"></i>ご注文の流れ</h2>
  <p class="mb30"><a href="<?php echo home_url(); ?>/flow"><img src="<?php bloginfo('template_url'); ?>/img/content_flow.jpg" width="760" alt="ご注文の流れ"></a></p>

</div><!-- .col-xs-13 -->

<?php get_footer(); ?>