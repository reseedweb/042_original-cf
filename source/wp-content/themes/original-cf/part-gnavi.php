
<nav class="cNav01 mt10">
  <ul class="clearfix">
  <li <?php if ( is_page('service') ) { echo ' class="current"'; } ?>>
  <a href="<?php bloginfo('url'); ?>/shape"><span>STEP1</span><br />形状を選ぶ</a></li>
  <li <?php if ( is_page('service') ) { echo ' class="current"'; } ?>>
  <a href="<?php bloginfo('url'); ?>/size"><span>STEP2</span><br />サイズを選ぶ</a></a></li>
  <li <?php if ( is_page('service') ) { echo ' class="current"'; } ?>>
  <a href="<?php bloginfo('url'); ?>/material"><span>STEP3</span><br />素材を選ぶ</a></a></li>
  <li <?php if ( is_page('service') ) { echo ' class="current"'; } ?>>
  <a href="<?php bloginfo('url'); ?>/print"><span>STEP4</span><br />印刷方法を選ぶ</a></a></li>
  <li <?php if ( is_page('service') ) { echo ' class="current"'; } ?>>
  <a href="<?php bloginfo('url'); ?>/option"><span>STEP5</span><br />オプションを選ぶ</a></a></li>
  <li <?php if ( is_page('service') ) { echo ' class="current"'; } ?>>
  <a href="<?php bloginfo('url'); ?>/price"><span>STEP6</span><br />参考価格例</a></a></li>
  </ul>
</nav>
