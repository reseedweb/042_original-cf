<?php
/*
Template Name: 404
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">
  
<?php get_template_part('part','bread'); ?>				


  <p>お探しのページは見つかりません。<br>
  URLをご確認の上、再読み込みするか、<a href="<?php echo home_url(); ?>">トップページ</a>へお戻り下さい。</p>

<?php get_template_part('part','contact'); ?>				


  </div><!-- .col-xs-13 -->

<?php get_footer(); ?>