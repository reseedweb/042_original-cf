<?php
/*
Template Name: fullOrder
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">
  
<?php get_template_part('part','bread'); ?>				


  <h2 class="content_h2 mt0"><i class="fa fa-circle-o"></i>完全フルオーダーでお作りします！</h2>

  <div class="cf">
  <img src="<?php bloginfo('template_url'); ?>/img/img_fullorder01.jpg" class="mr20 pb20 pull-left" alt="">
  <p class="pull-right fullorder_txt01">フルオーダーでのクリアファイル制作なら！サイズや印刷デザインはもちろん、素材・表面加工等すべてお選びいただくことが可能です。<br />
  好みのイメージやご予算をお伝えいただければ、お客様のご要望に沿って最適なご提案をいたします。イベント・企業の販促グッズ・ノベルティグッズ・資料の配布用などでよくご利用いただいております。<br />
  版代・送料無料のキャンペーンも行っておりますので、 この機会に是非ご活用ください！</p>
  </div><!-- /cf -->
  
  <div class="cf">
	<figure class="pb20 pull-left">
  <img src="<?php bloginfo('template_url'); ?>/img/img_fullorder02.jpg" alt="企業の販促グッズ・資料配布用に！">
  <figcaption class="mt10">
  企業の販促グッズ・資料配布用に！
  </figcaption>
  </figure>

	<figure class="pb20 pull-left">
  <img src="<?php bloginfo('template_url'); ?>/img/img_fullorder03.jpg" alt="サイズは自由に選べます！">
  <figcaption class="mt10">
  サイズは自由に選べます！
  </figcaption>
  </figure>

	<figure class="mr0 pb20 pull-left">
  <img src="<?php bloginfo('template_url'); ?>/img/img_fullorder04.jpg" alt="ホログラムや圧着加工も可能です！">
  <figcaption class="mt10">
  ホログラムや圧着加工も可能です！
  </figcaption>
  </figure>

  </div><!-- /cf -->

  <h3 class="mt30">フルオーダーはこんな方におすすめです</h3>
  <div class="cf pt20 fullorder_txt02">
  <img src="<?php bloginfo('template_url'); ?>/img/img_fullorder05.jpg" class="mr20 pb20 pull-left" alt="">
  <p class="red pull-right">■ 用途に合わせて実用性の高いグッズを作りたい！</p>
  <p class="pull-right">オリジナルクリアファイルは、イベントでの資料配布やノベルティグッズ・販促PRを目的として制作されることが多くあります。実用性の高いクリアファイルなら、長く使っていただけ販促効果もアップです！</p>
  <p class="red pull-right">■ 表面加工やオプションで他との差をつけたい！</p>
  <p class="pull-right">フルオーダーなら形状・サイズをご自由にカスタマイズできるだけでなく、エンボスやホログラム加工などの多様なオプションをつけることも可能です。他にはないインパクト抜群のクリアファイルを作成できます！</p>
  <p class="red pull-right">■ 印刷やデザイン表現にこだわりたい！</p>
  <p class="pull-right">オリジナルクリアファイルへのカラー印刷はもちろんのこと、写真やグラデーションの表現・豪華な箔押しでの名入れも可能です。イラスト・デザインを鮮やかに表現でき、イベントグッズなどにもよく使われています。</p>
  </div>
  
  
  
  <h2 class="content_h2"><i class="fa fa-circle-o"></i>はじめての方でも選ぶだけで簡単お見積もり！</h2>
  <p>オリジナルクリアファイルWebでは、初めてショップ紙袋を制作されるお客様にも選んでいただきやすいよう、ステップオーダー方式を採用しております。より詳細の情報が知りたいなど疑問点ありましたら、スタッフまでお気軽にお問い合わせください。</p>

	<h3>フルオーダーで選べる仕様</h3>
  <table class="fullorder_table">
  <tr class="semi-white">
  <th>形状を選ぶ</th>
  <td>ノーマル/ポケット付き/ロングポケット付き/仕切り付き/<br />
  封筒型ファイル縦/封筒型ファイル横/バッグ型ファイル縦/<br />
  バッグ型ファイル横</td>
  <td><a href="<?php bloginfo('url'); ?>/shape"><img src="<?php bloginfo('template_url'); ?>/img/fullOrder_btn.png" alt="詳しくはこちら"></a></td>
  </tr>
  <tr class="non-color">
  <th>サイズを選ぶ</th>
  <td>A4/B5/A5/A6/オリジナルサイズ</td>
  <td><a href="<?php bloginfo('url'); ?>/size"><img src="<?php bloginfo('template_url'); ?>/img/fullOrder_btn.png" alt="詳しくはこちら"></a></td>
  </tr>
  <tr class="semi-white">
  <th>素材を選ぶ</th>
  <td>ナチュラルPP/マットPP/高透明PP</td>
  <td><a href="<?php bloginfo('url'); ?>/material"><img src="<?php bloginfo('template_url'); ?>/img/fullOrder_btn.png" alt="詳しくはこちら"></a></td>
  </tr>
  <tr class="non-color">
  <th>印刷方法を選ぶ</th>
  <td>1色印刷/２色印刷/４色フルカラー/箔押し</td>
  <td><a href="<?php bloginfo('url'); ?>/print"><img src="<?php bloginfo('template_url'); ?>/img/fullOrder_btn.png" alt="詳しくはこちら"></a></td>
  </tr>
  <tr class="semi-white">
  <th>オプションを選ぶ</th>
  <td>エンボス加工/ホログラム加工/圧着加工/<br />
  名刺ポケット付き/OPP袋封入/なし</td>
  <td><a href="<?php bloginfo('url'); ?>/option"><img src="<?php bloginfo('template_url'); ?>/img/fullOrder_btn.png" alt="詳しくはこちら"></a></td>
  </tr>
  </table>
  
  <p class="mt50">
  <a href="<?php echo home_url(); ?>/price#price_full" class="mr10"><img src="<?php bloginfo('template_url'); ?>/img/btn_fullorderPrice.jpg" alt="フルオーダーの参考価格例を見る"></a>
  <a class="ml5" href="<?php echo home_url(); ?>/estimation"><img src="<?php bloginfo('template_url'); ?>/img/btn_fullorderEst.jpg" alt="フルオーダーのお見積もりはコチラから"></a>
  </p>


<?php get_template_part('part','contact'); ?>				

  
</div><!-- .col-xs-13 -->

<?php get_footer(); ?>