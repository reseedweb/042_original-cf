<?php
/*
Template Name: price
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">
  
<?php get_template_part('part','bread'); ?>				


  <h2 class="content_h2 mt0" id="price_full"><i class="fa fa-circle-o"></i>オリジナルクリアファイルの参考価格例</h2>
  <p>オリジナルクリアファイルの制作にどれぐらいの価格がかかるんだろう？という疑問にお応えするため、わかりやすい参考価格例を掲載しています。<br />
  あくまで参考の価格になります。<br />
  実際のお見積もり価格はクリアファイルの仕様により変動します。<br />
  詳しくはお電話か見積もりフォームにてお問い合わせください。</p>
  
    <h3>４色フルカラー印刷</h3>
  <div class="standard-box cf mb30">
      <p>透明なクリアファイルに４色フルカラーで全面印刷した場合の参考価格例です。<br />
      ※下記のサイズ・枚数でご注文した際の１枚あたりの単価を表記しています。（オプションなし）<br />
      ※価格は全て税抜き表示となっております。</p>
		<table class="price-table">
			<tr>
				<th>サイズ</th>
				<th>300枚</th>
				<th>500枚</th>
				<th>1,000枚</th>
				<th>3,000枚</th>
				<th>5,000枚</th>
               <th>10,000枚</th>
			</tr>
			<tr>
				<td>A4サイズ</td>
				<td class="price_font">¥123</td>
				<td class="price_font">¥84</td>
				<td class="price_font">¥54</td>
				<td class="price_font">¥33</td>
				<td class="price_font">¥29</td>
               <td class="price_font">¥26</td>
			</tr>
            
			<tr>
				<td>B5サイズ</td>
				<td class="price_font">¥167</td>
				<td class="price_font">¥110</td>
				<td class="price_font">¥67</td>
				<td class="price_font">¥37</td>
				<td class="price_font">¥32</td>
               <td class="price_font">¥27</td>
			</tr>
			<tr>
				<td>A5サイズ</td>
				<td class="price_font">¥157</td>
				<td class="price_font">¥100</td>
				<td class="price_font">¥56</td>
				<td class="price_font">¥28</td>
				<td class="price_font">¥22</td>
               <td class="price_font">¥18</td>
			</tr>
			<tr>
				<td>A6サイズ</td>
				<td class="price_font">¥156</td>
				<td class="price_font">¥98</td>
				<td class="price_font">¥56</td>
				<td class="price_font">¥28</td>
				<td class="price_font">¥22</td>
              <td class="price_font">¥18</td>
			</tr>
		</table>
  </div><!-- standard-box -->

  <h2 class="content_h2 mt0" id="price_name"><i class="fa fa-circle-o"></i>名入れクリアファイルの参考価格例</h2>
  
  <h3>１色印刷</h3>
  <div class="standard-box cf mb30">
      <p>透明なクリアファイルに１色でデザインを印刷した場合の参考価格例です。<br />
      ※下記のサイズ・枚数でご注文した際の１枚あたりの単価を表記しています。（オプションなし）<br />
      ※価格は全て税抜き表示となっております。</p>
		<table class="price-table">
			<tr>
				<th>サイズ</th>
				<th>300枚</th>
				<th>500枚</th>
				<th>1,000枚</th>
				<th>3,000枚</th>
				<th>5,000枚</th>
               <th>10,000枚</th>
			</tr>
			<tr>
				<td>A4サイズ</td>
				<td class="price_font">¥90</td>
				<td class="price_font">¥60</td>
				<td class="price_font">¥40</td>
				<td class="price_font">¥26</td>
				<td class="price_font">¥22</td>
               <td class="price_font">¥19</td>
			</tr>
           <!--
			<tr>
				<td>B5サイズ</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
			</tr>
			<tr>
				<td>A5サイズ</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
			</tr>
			<tr>
				<td>A6サイズ</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
			</tr>-->
		</table>
  </div><!-- standard-box -->
  
  
  <h3>箔押し</h3>
  <div class="standard-box cf mb30">
      <p>透明なクリアファイルに企業名などを箔押しした場合の参考価格例です。<br />
      ※下記のサイズ・枚数でご注文した際の１枚あたりの単価を表記しています。（オプションなし）<br />
      ※価格は全て税抜き表示となっております。</p>
		<table class="price-table">
			<tr>
				<th>サイズ</th>
				<th>300枚</th>
				<th>500枚</th>
				<th>1,000枚</th>
				<th>3,000枚</th>
				<th>5,000枚</th>
               <th>10,000枚</th>
			</tr>
			<tr>
				<td>A4サイズ</td>
				<td class="price_font">¥115</td>
				<td class="price_font">¥31</td>
				<td class="price_font">¥23</td>
				<td class="price_font">¥17</td>
				<td class="price_font">¥14</td>
                <td class="price_font">¥13</td>
			</tr>
            <!--
			<tr>
				<td>B5サイズ</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
			</tr>
			<tr>
				<td>A5サイズ</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
			</tr>
			<tr>
				<td>A6サイズ</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
				<td class="price_font">¥0</td>
			</tr>-->
		</table>
  </div><!-- standard-box -->
  


<div class="pager mt50">
<ul>
<li class="prev mr20"><a class="mr10" href="<?php bloginfo('url'); ?>/option"><i class="fa fa-sort-desc fa-rotate-90"></i>STEP5 オプションを選ぶ</a></li>
<li class="next mr30"><a class="mr10" href="<?php bloginfo('url'); ?>/estimation">お見積もりはこちらから<i class="fa fa-sort-asc fa-rotate-90"></i></a></li>
</ul>
</div>
 
    
<?php get_template_part('part','contact'); ?>				


    </div><!-- .col-xs-13 -->
	</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>    
    
