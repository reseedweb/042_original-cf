<?php
/*
Template Name: estimation01
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">

	<?php get_template_part('part','bread'); ?>
    
			<h2 class="content_h2">送信されました</h2>
            <div>
    <p>オリジナルクリアファイルWEBにお問い合わせ頂き、誠にありがとうございます。</p>
    <p>メールは無事送信されました。<br>
    弊社の専門スタッフが１営業日以内に内容を確認し、ご返信させて頂きます。<br>
    もし、３営業日経っても返信がない場合は、メールがきちんと送信されていない場合がございます。<br>
    お手数お掛けしますが、もう一度ご送信頂くか、下記のお問い合わせ先までご連絡下さいませ。</p>

    <p>お急ぎの方はこちらまで（受付時間/月～金曜 9:00～20:00　土曜 10:00～17:00）<br>
    TEL：0120-838-117</p>
 
</div>
<h2 class="content_h2"><i class="fa fa-circle-o"></i>ご注文の流れ</h2>
  <p class="mb30"><a href="<?php echo home_url(); ?>/flow"><img src="<?php bloginfo('template_url'); ?>/img/content_flow.jpg" width="760"></a></p>


    </div><!-- .col-xs-13 -->
</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>