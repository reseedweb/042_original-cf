<?php
/*
Template Name: size
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">
  
<?php get_template_part('part','bread'); ?>				


  <h2 class="content_h2 mt0"><i class="fa fa-circle-o"></i>クリアファイルのサイズを選ぶ</h2>
  <p>オリジナルクリアファイルのサイズを、A4・B5・A5・A6のなかから用途に合わせてお選びいただけます。<br />
  また、よりオリジナリティーのあるファイルを制作されたい方は、既成サイズ以外の完全オリジナルサイズでの制作も可能です。<br />
  海外国内に提携工場を保有しているからこそ実現できる、低価格&amp;高品質のクリアファイルを是非お試しください！</p>
  
    
  <h3 id="1">A4クリアフアィル</h3>
  <div class="standard-box cf mb30">
  <img src="<?php bloginfo('template_url'); ?>/img/img_size01.jpg" alt="A4クリアフアィル">
  <div class="standard-box-txt size-txt">
  <h4>特長</h4>
  <p>A4サイズ：縦310mm×横220mm<br />
  もっとも一般的なA4サイズのクリアファイルです。<br />
  イベントでの配布や企業のノベルティグッズなど、様々な場面でお使いいただけます。<br />
  使いやすいので、配布後に使用してもらえる可能性も高いです。<br />
  クリアファイルの全面に印刷できます。</p>
  </div><!-- standard-box-txt -->
  </div><!-- standard-box -->

  <h3 id="2">B5クリアファイル</h3>
  <div class="standard-box cf mb30">
  <img src="<?php bloginfo('template_url'); ?>/img/img_size02.jpg" alt="B5クリアファイル">
  <div class="standard-box-txt size-txt">
  <h4>特長</h4>
  <p>B5サイズ：縦271mm×横193mm<br />
  教科書やノートのサイズにもあったB5サイズです。<br />
  学生さんによく使用していただけるので、学校関係のイベントやオープンキャンパスでの配布用にも選ばれています。<br />
  クリアファイルの全面に印刷できます。</p>
  </div><!-- standard-box-txt -->
  </div><!-- standard-box -->
  
  <h3 id="3">A5クリアファイル</h3>
  <div class="standard-box cf mb30">
  <img src="<?php bloginfo('template_url'); ?>/img/img_size03.jpg" alt="A5クリアファイル">
  <div class="standard-box-txt size-txt">
  <h4>特長</h4>
  <p>A5サイズ：縦220mm×横153mm<br />
  少し小さめのA5サイズのクリアファイルです。<br />
  ちょっとした資料やメモなどを入れるのに便利です。企業のノベルティグッズやおまけとしても活用されています。<br />
  クリアファイルの全面に印刷できます。</p>
  </div><!-- standard-box-txt -->
  </div><!-- standard-box -->
  
  <h3 id="4">A6クリアファイル</h3>
  <div class="standard-box cf mb30">
  <img src="<?php bloginfo('template_url'); ?>/img/img_size04.jpg" alt="A6クリアファイル">
  <div class="standard-box-txt size-txt">
  <h4>特長</h4>
  <p>A6サイズ：縦158mm×横110mm<br />
  ハガキや写真などを収納するのにピッタリな、A6サイズのクリアファイルです。<br />
  小さめの鞄にも収納でき、持ち運びに便利です。企業のノベルティグッズやおまけとしても活用されています。<br />
  クリアファイルの全面に印刷できます。</p>
  </div><!-- standard-box-txt -->
  </div><!-- standard-box -->
  
  <h3 id="5">オリジナルサイズ</h3>
  <div class="standard-box cf mb40">
  <p>A4・B5・A5・A6以外のサイズでのクリアファイル制作をご希望の方は、完全オリジナルサイズでの制作も可能です。<br />
  より用途にあわせて、インパクトのあるオリジナルクリアファイルが制作できます。ただし、機械の性能上、あまりにも大きすぎるものは作成できない場合がございます。<br />
  まずはご希望のサイズを、お見積もりもしくはお問い合わせ時にスタッフまでご相談ください。</p>
  </div><!-- standard-box -->
  
        
  <div class="pager">
  <ul>
    <li class="prev mr20"><a href="<?php echo home_url(); ?>/shape" class="mr10"><i class="fa fa-sort-desc fa-rotate-90"></i>STEP1 形状を選ぶ</a></li>
    <li class="next mr30"><a href="<?php echo home_url(); ?>/material">STEP3 素材を選ぶ<i class="fa fa-sort-asc fa-rotate-90"></i></a></li>
  </ul>
  </div>
  
  
<?php get_template_part('part','contact'); ?>				

  
</div><!-- .col-xs-13 -->

<?php get_footer(); ?>