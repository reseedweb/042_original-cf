<?php
/*
Template Name: about
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">
  
<?php get_template_part('part','bread'); ?>				


  <div class="about01">
  <h2 class="content_h2 mt0"><i class="fa fa-circle-o"></i>オリジナルクリアファイルWebは対応力が違います！</h2>
  <img src="<?php bloginfo('template_url'); ?>/img/img_about01.jpg" alt="オリジナルクリアファイルWebは対応力が違います！">
  <p>オリジナルクリアファイルWebでは、お客様からご依頼頂いてから5分以内に電話、５分以内に見積もりを行える体制を徹底して行っております。お客様の良きパートナーになれるようにスタッフの教育には力を入れて取り組んでおります。「見積もりの正確さ、スピード、対応力、価格面」ではどこにも負けないと自信があります。</p>
  </div>
  
  <div class="about02">
  <img src="<?php bloginfo('template_url'); ?>/img/img_about02.jpg" alt="完全オリジナルサイズ&デザイン">
  <p>オリジナルクリアファイルWebでは、宣伝効果の高いオリジナルの印刷をしたクリアファイルの制作をご提案しております。サイズだけではなく素材・印刷・加工の方法までお好きにカスタマイズできます。またご自身でデザインが難しい場合は弊社デザイナーがオリジナルデザインの制作も承ります（別途費用）。</p>
  </div>
  
  <div class="about03 mb30">
  <img src="<?php bloginfo('template_url'); ?>/img/img_about03.jpg" alt="来社される方は直接対応">
  <p>ネットで注文できる時代とはいえ、電話だけでは不安という方もいらっることでしょう。そういう時には来社いただくことも可能です。担当スタッフが直接お話をお伺いし、過去の制作事例など実物をお見せしながらご提案させていたきます。場所によってはコチラからお伺いすることも可能です。お気軽にご相談ください。</p>
  </div>
  

  <h2 class="content_h2 mb20"><i class="fa fa-circle-o"></i>サービスのご案内</h2>
  
  <ul class="mb30 pl0 clearfix">
  <li class="thumbnail mr10">
  <div> 
  <p class="selvice_ttl">クリアファイルの企画･制作</p>
  <img class="selvice_img" src="<?php bloginfo('template_url'); ?>/img/about_img01.jpg" alt="クリアファイルの企画･制作">
  <p class="mt10">お客様のご要望に合わせて、オリジナルクリアファイルを制作します。一般的な形状だけでなく、便利なポケット付きや珍しい封筒型・バッグ型のものなど多種多用なクリアフィルの制作が可能です。</p>
  <a href="<?php echo home_url(); ?>/fullorder" class="btn">フルオーダークリアファイル</a>
  </div>
  </li>
  <li class="thumbnail mr10">
  <div>
  <p class="selvice_ttl">リピーター割引</p>
  <img class="selvice_img" src="<?php bloginfo('template_url'); ?>/img/about_img02.jpg" alt="リピーター割引">
  <p class="mt10">クリアファイルの制作には、印刷に必要な版を制作するための「版代」が必ず発生します。リピーター様の場合はデザインが同じであれば版が繰り返し使えるので、版代がかからずお安く制作することができます。</p>
  <a href="<?php echo home_url(); ?>/contact" class="btn">お問い合わせ</a>
  </div>
  </li>
  <li class="thumbnail">
  <div>
  <p class="selvice_ttl">ファイル･ロゴデザイン制作</p>
  <img class="selvice_img" src="<?php bloginfo('template_url'); ?>/img/about_img03.jpg" alt="ファイル･ロゴデザイン制作">
  <p class="mt10">デザインや入稿データをお客様自身で作ることが難しい場合は、弊社の専任デザイナーが代わってオリジナルデザインの作成を承ることも可能です（別途費用）。企業のロゴデザインなどもお任せください。</p>
  <a href="<?php echo home_url(); ?>/design" class="btn">オリジナルデザイン制作</a>
  </div>
  </li>
  </ul>

  
  <p class="mb30"><a target="_blank" href="http://original-pb.net/"><img src="<?php bloginfo('template_url'); ?>/img/top_bnr05.jpg" alt="ポリ袋・紙袋を一緒にご注文で最大20%OFF"></a></p>

  <h2 id="novelty" class="content_h2"><i class="fa fa-circle-o"></i>ノベルティーグッズ全般制作できます！</h2>
	<div class="cf mb10">
  <p class="pull-left"><img src="<?php bloginfo('template_url'); ?>/img/img_about04.jpg" alt="オリジナルグッズ・ノベルティグッズ"></p>
  <p class="pull-right novelty_txt mt10">
  <span>オリジナルグッズ･ノベルティグッズ<br />
  ならRERECAにお任せください！</span><br /><br />
  オリジナルクリアファイルWebを運用しているRERECAは、他にも様々な販促物やノベルティグッズの企画・制作を承っております。海外・国内に多数の提携工場を保有しており、取り扱い商品は並べきれません。どんな商品でもまずは販促グッズ制作のRERECAまでご相談ください。</p>
  </div>

  <h3>例えばこんなグッズが制作できます</h3>
	<div class="cf mb10">
  <figure class="pull-left">
  <img src="<?php bloginfo('template_url'); ?>/img/img_about05.jpg" alt="ビニールポーチ">
  <figcaption>ビニールポーチ</figcaption>
  </figure>
  <figure class="pull-left">
  <img src="<?php bloginfo('template_url'); ?>/img/img_about06.jpg" alt="パッケージ">
  <figcaption>パッケージ</figcaption>
  </figure>
  <figure class="pull-left mr0">
  <img src="<?php bloginfo('template_url'); ?>/img/img_about07.jpg" alt="紙袋">
  <figcaption>紙袋</figcaption>
  </figure>
	</div>
  <p class="mb30">紙袋・ポリ袋・不織布袋・エコバッグ・保冷バッグ・ビニールポーチ・横断幕・宅配袋・宅配ケース・包装資材・段ボール・パッケージ・化粧箱・クリアケース・下敷き・シール・ステッカー・キーホルダー・ストラップ・メモ帳・便箋・冊子・缶バッジ・ピンバッジ・釦・社章・スマホケース・Tシャツ・パンツ・ステーショナリー・Webサイト制作…etc</p>

<?php get_template_part('part','contact'); ?>				
  
</div><!-- .col-xs-13 -->
  
  <?php get_footer(); ?>