<?php
/*
Template Name: material
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 subpage">

<?php get_template_part('part','bread'); ?>				


  <h2 class="content_h2 mt0"><i class="fa fa-circle-o"></i>クリアファイルの素材を選ぶ</h2>
  <p>オリジナルクリアファイルの素材・質感を好みに合わせて選択できます。<br />
  手になじむナチュラルや、高級感のあるマットPP、高透明PPなど…どれもお勧めのラインナップを揃えています。<br />
  「素材についてよくわからない」「どれを選べばよいか迷ってしまう」という方は、是非スタッフまでお気軽にご相談ください。ご要望をお聞きし、最適なものをご提案いたします。</p>
  
  <h3>ナチュラルPP</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_material01.jpg" alt="ナチュラルPP" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>手になじむベーシックな素材を使用したクリアファイルです。<br />
      多用途に使用でき、比較的価格も安価に制作できます。<br />
      なるべく費用を抑えてオリジナルクリアファイルを制作したいという方にはお勧めです。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->
  
  
  <h3>マットPP</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_material02.jpg" alt="マットPP" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>クリアファイル表面のつやを抑えた、マットPP仕様のクリアファイルです。<br />
      ]表面は光沢がなく非常に高級感のある質感になります。<br />
      表面が反射しにくくなるので、イラストやデザインを綺麗に見せたい方に特に選ばれています。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->
  
  <h3>高透明PP</h3>
  <div class="standard-box cf mb30">
    <img src="<?php bloginfo('template_url'); ?>/img/img_material03.jpg" alt="高透明PP" >
    <div class="standard-box-txt">
    <h4>特長</h4>
      <p>非常に透明度の高いオリジナルクリアファルが作成できます。<br />
      化粧品のパッケージなどに使われるようなフィルムで、中に入れた書類がクリアに見えます。<br />
      配布物をきれいに見せたい場合や、クリアファイルに入れたまま展示をする場合にも活躍します。</p>
    </div><!-- standard-box-txt -->
  </div><!-- standard-box -->


<div class="pager mt50">
<ul>
<li class="prev mr20"><a class="mr10" href="<?php bloginfo('url'); ?>/size"><i class="fa fa-sort-desc fa-rotate-90"></i>STEP2 サイズを選ぶ</a></li>
<li class="next mr30"><a class="mr10" href="<?php bloginfo('url'); ?>/print">STEP4 印刷方法を選ぶ<i class="fa fa-sort-asc fa-rotate-90"></i></a></li>
</ul>
</div>
 
    
<?php get_template_part('part','contact'); ?>				


    </div><!-- .col-xs-13 -->
	</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>