<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="col-xs-13 home">

  <h2 class="content_h2 h2_first01 mt0"><i class="fa fa-circle-o"></i>形状から選ぶ</h2>

<div class="row">

    <div class="col-xs-4 lineup">
    <a class="thumbnail punder" href="<?php bloginfo('url'); ?>/shape#1">
    <figure class="txtCeter">
    <img src="<?php bloginfo('template_url'); ?>/img/top_img_figure01.jpg" alt="">
		<figcaption>
    ノーマル
    </figcaption>
    </figure>
    </a>
    </div> 

    <div class="col-xs-4 lineup">
    <a class="thumbnail punder" href="<?php bloginfo('url'); ?>/shape#2">
    <figure>
    <img src="<?php bloginfo('template_url'); ?>/img/top_img_figure02.jpg" alt="">
		<figcaption>
		ポケット付き
    </figcaption>
    </figure>
    </a>
    </div> 

    <div class="col-xs-4 lineup">
    <a class="thumbnail punder" href="<?php bloginfo('url'); ?>/shape#3">
    <figure>
    <img src="<?php bloginfo('template_url'); ?>/img/top_img_figure03.jpg" alt="">
		<figcaption>
		ロングポケット付き
    </figcaption>
    </figure>
    </a>
    </div> 

    <div class="col-xs-4 lineup">
    <a class="thumbnail punder" href="<?php bloginfo('url'); ?>/shape#4">
    <figure>
    <img src="<?php bloginfo('template_url'); ?>/img/top_img_figure04.jpg" alt="">
		<figcaption>
		仕切り付き
    </figcaption>
    </figure>
    </a>
    </div> 

</div><!-- /row -->

<div class="row mb30">

    <div class="col-xs-4 lineup">
    <a class="thumbnail punder" href="<?php bloginfo('url'); ?>/shape#5">
    <figure>
    <img src="<?php bloginfo('template_url'); ?>/img/top_img_figure05.jpg" alt="">
		<figcaption>
		封筒型ファイル縦
    </figcaption>
    </figure>
    </a>
    </div> 

    <div class="col-xs-4 lineup">
    <a class="thumbnail punder" href="<?php bloginfo('url'); ?>/shape#6">
    <figure>
    <img src="<?php bloginfo('template_url'); ?>/img/top_img_figure06.jpg" alt="">
		<figcaption>
		封筒型ファイル横
    </figcaption>
    </figure>
    </a>
    </div> 

    <div class="col-xs-4 lineup">
    <a class="thumbnail punder" href="<?php bloginfo('url'); ?>/shape#7">
    <figure>
    <img src="<?php bloginfo('template_url'); ?>/img/top_img_figure07.jpg" alt="">
		<figcaption>
		バッグ型ファイル縦
    </figcaption>
    </figure>
    </a>
    </div> 

    <div class="col-xs-4 lineup">
    <a class="thumbnail punder" href="<?php bloginfo('url'); ?>/shape#8">
    <figure>
    <img src="<?php bloginfo('template_url'); ?>/img/top_img_figure08.jpg" alt="">
		<figcaption>
		バッグ型ファイル横
    </figcaption>
    </figure>
    </a>
    </div> 

</div><!-- /row -->

  <h2 class="content_h2 h2_first01"><i class="fa fa-circle-o"></i>サイズから選ぶ</h2>

<div class="row">

    <div class="col-xs-4 lineup">
    <a class="thumbnail punder" href="<?php bloginfo('url'); ?>/size#1">
    <figure class="txtCeter">
    <img src="<?php bloginfo('template_url'); ?>/img/top_img_figure09.jpg" alt="">
		<figcaption>
    A4クリアファイル
    </figcaption>
    </figure>
    </a>
    </div> 

    <div class="col-xs-4 lineup">
    <a class="thumbnail punder" href="<?php bloginfo('url'); ?>/size#2">
    <figure>
    <img src="<?php bloginfo('template_url'); ?>/img/top_img_figure10.jpg" alt="">
		<figcaption>
		B5クリアファイル
    </figcaption>
    </figure>
    </a>
    </div> 

    <div class="col-xs-4 lineup">
    <a class="thumbnail punder" href="<?php bloginfo('url'); ?>/size#3">
    <figure>
    <img src="<?php bloginfo('template_url'); ?>/img/top_img_figure11.jpg" alt="">
		<figcaption>
		A5クリアファイル
    </figcaption>
    </figure>
    </a>
    </div> 

    <div class="col-xs-4 lineup">
    <a class="thumbnail punder" href="<?php bloginfo('url'); ?>/size#4">
    <figure>
    <img src="<?php bloginfo('template_url'); ?>/img/top_img_figure12.jpg" alt="">
		<figcaption>
		A6クリアファイル
    </figcaption>
    </figure>
    </a>
    </div> 

</div><!-- /row -->

<p class="mb40 mt20"><a href="<?php bloginfo('url'); ?>/price"><img src="<?php bloginfo('template_url'); ?>/img/top_bnr01.jpg" alt=""></a></p>


    <h2 class="content_h2 h2_first01"><i class="fa fa-circle-o"></i>お勧め商材ラインナップ</h2>

    <p class="mt20"><a href="<?php bloginfo('url'); ?>/nameorder"><img src="<?php bloginfo('template_url'); ?>/img/top_bnr02.jpg" alt="箔押し・名入れ印刷クリアファイルが手軽で安い!!">
    <span>オリジナルショッパーを置く場所がない…とお困りの方へ！最長１年間、弊社の提携倉庫にて在庫の保管可能です！</span></a></p>

		<!--<div class="cf">
    <p class="mt10 mr20 pull-left"><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/img/top_bnr03.jpg" alt="企業名・名入れ印刷クリアファイル"><br />
    <span>企業名・名入れ印刷可能なクリアブック</span></a></p>

    <p class="mt10"><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/img/top_bnr04.jpg" alt="カラーが選べる!レール付きクリアフォルダー"><br />
    <span>カラーが選べるレール付きクリアフォルダー</span></a></p>
    </div> -->

    <p class="mt10 mb40"><a target="_blank" href="http://original-pb.net/"><img src="<?php bloginfo('template_url'); ?>/img/top_bnr05.jpg" alt="ポリ袋・紙袋を一緒にご注文で最大20%OFF">
    <span>オリジナルクリアファイルを置く場所がない…とお困りの方へ！最長１年間、弊社の提携倉庫にて在庫の保管可能です！
</span></a></p>


    <h2 class="content_h2 h2_first01"><i class="fa fa-circle-o"></i>オリジナルクリアファイルWebは対応力が違います！</h2>

    <a href="<?php bloginfo('url'); ?>/about"><img src="<?php bloginfo('template_url'); ?>/img/content_5min.jpg" height="280" width="761" alt="オリジナルクリアファイルWebは対応力が違います！"></a>


    <h2 class="content_h2"><i class="fa fa-circle-o"></i>データ入稿・デザインについて</h2>

    <div class="cf pa20 topDesignBox">
    <div class="top_design">
    <h3>ご注文後デザインデータを作成し<br>データ入稿をお願い致します。</h3>
    <p>クリアファイルのテンプレートに合わせてデザインしていただく必要があります。<br />
    各サイズのテンプレートは以下リンク先よりダウンロード、その他オリジナルサイズについては担当スタッフからお送りさせていただきます。<br />
    デザインが出来ましたら、<a href="mailto:info@original-cf.net"> info@original-cf.net </a>へご連絡ください。</p>
    <p class="btnmg01"><a href="<?php bloginfo('url'); ?>/draft"><img src="<?php bloginfo('template_url'); ?>/img/content_nyuko_btn.png" width="406" alt="入稿について詳しくはコチラ"></a></p>
    </div>
    </div>

    <p class="mt20"><a href="<?php bloginfo('url'); ?>/design"><img src="<?php bloginfo('template_url'); ?>/img/top_bnr06.jpg" alt="デザインが苦手な方もご安心ください！ファイルデザインロゴ制作"></a></a>


	<h2 class="content_h2"><i class="fa fa-circle-o"></i>
ご注文の流れ</h2>
	<p><a href="<?php bloginfo('url'); ?>/flow"><img src="<?php bloginfo('template_url'); ?>/img/content_flow.jpg" width="760" alt="ご注文の流れ"></a></p>

  <div class="content_contact">
    <p><img src="<?php bloginfo('template_url'); ?>/img/content_tel.png" alt="お問い合わせお見積もりはこちらから。メールは24時間受付"></p>
    <p><a href="<?php bloginfo('url'); ?>/contact"><img src="<?php bloginfo('template_url'); ?>/img/content_con.png" height="47" width="256" alt="お問い合わせはこちら"></a>
    <a href="<?php bloginfo('url'); ?>/estimation"><img src="<?php bloginfo('template_url'); ?>/img/content_est.png" height="47" width="256" alt="お見積もりはこちら"></a></p>
  </div>


	<h2 class="content_h2"><i class="fa fa-circle-o"></i>
オリジナルクリアファイルWEBからのご挨拶</h2>
	<p class="pull-right pl20 pb20"><img src="<?php bloginfo('template_url'); ?>/img/content_aisatu_img.jpg" alt="クリアファイルWeb"></p>
	<p class="lheight180">オリジナルクリアファイルWebは企業様や店舗様に対し、目的に合わせた様々な用途に使用できるオリジナル仕様のショップ向け紙袋・ポリ袋・不織布袋制作からデザイン作成から名入れ・印刷・お届けまで、弊社スタッフが安心サポートにてお手伝いいたします。<br /><br />
  
  紙袋・ポリ袋・不織布袋の素材やサイズ、印刷色数まで自由に選択でき、名入れやオリジナル・デザインを印刷することで、ユーザーへの広告宣伝やブランド・イメージ向上の効果が発揮できでき、またユーザーでのリユース活用により継続的な宣伝効果が見込めます。<br /><br />
  
  オリジナルクリアファイルWebではお客様に対し、2つのサービスプランをご提案し、それぞれ「価格優先」、「納期優先」といったお客様のニーズに合ったオリジナルショッパーを格安にてご対応させていただいております。</p>
  
  <p class="mt20 lheight180">長年培ってきた海外・国内印刷工場のネットワークを元に、信頼できる制作品質と印刷技術、豊富な素材を持った工場を利用させて頂くことになりました。オリジナルショッパー・ショップバッグを使用した販促物でのコスト削減をお考えのお客様はぜひ一度、オリジナルショッパーWebでご相談くださいませ。</p>

  </div><!-- .col-xs-13 -->

<?php get_footer(); ?>