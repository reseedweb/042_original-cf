<!DOCTYPE html>
<html>
<head>
    <!-- meta -->        
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />         
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, maximum-scale=1.0, user-scalable=yes">
    <!-- title -->
    <title><?php wp_title('|', true, 'right'); bloginfo('name');?></title>
    <meta name="robots" content="noindex,follow,noodp" />
    
    <link rel="profile" href="http://gmpg.org/xfn/11" />                
    
    <!-- global javascript variable -->
    <script type="text/javascript">
        var CONTAINER_WIDTH = '1090px';
        var CONTENT_WIDTH = '1060px';
        var BASE_URL = '<?php bloginfo('url'); ?>';
        var TEMPLATE_URI = '<?php bloginfo('template_url') ?>';
        var CURRENT_MODULE_URI = '';
        Date.now = Date.now || function() { return +new Date; };            
    </script>        

    <!-- Bootstrap -->
    <link href="<?php bloginfo('template_url'); ?>/css/bootstrap-theme.css" rel="stylesheet" />   
    <!-- fontawesome -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
    <![endif]-->

	<script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js" type="text/javascript"></script>
	<?php if(is_home()) : ?>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.flexslider-min.js"></script>
    <link href="<?php bloginfo('template_url'); ?>/css/flexslider.css" rel="stylesheet" />
	<?php elseif(is_page('estimation01')) : ?>																				
    <script src="<?php bloginfo('template_url'); ?>/js/script.js"></script>
	<?php elseif(is_page('price_normal')) : ?>	
	<script src="<?php bloginfo('template_url'); ?>/js/config.js"></script>
	<?php else :?>			
	<?php endif; ?>
    <link href="<?php bloginfo('template_url'); ?>/style.css?<?php echo md5(date('l jS \of F Y h:i:s A')); ?>" rel="stylesheet" />

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header>
  <div class="container">
    <div class="row">
      <div class="col-xs-5">
      <h1 class="header_h1">オリジナル・名入れの<br>
      クリアファイル印刷・制作なら</h1>
      <a href="<?php echo home_url(); ?>">
      <img src="<?php bloginfo('template_url'); ?>/img/common/header_logo.jpg" height="185" width="300" alt="オリジナルエコバッグWEB"></a>
			</div><!-- /col-xs-5 -->

      <div class="col-xs-13">
      <nav>
      <ul class="list-inline header_nav pull-right">
      <li><a href="<?php echo home_url(); ?>/about"><img src="<?php bloginfo('template_url'); ?>/img/common/header_nav01.jpg" width="145" height="55" alt="初めての方へ"></a></li>
      <li><a href="<?php bloginfo('url'); ?>/flow"><img src="<?php bloginfo('template_url'); ?>/img/common/header_nav02.jpg" width="145" height="55" alt="ご注文の流れ"></a></li>
      <li><a href="<?php bloginfo('url'); ?>/draft"><img src="<?php bloginfo('template_url'); ?>/img/common/header_nav03.jpg" width="145" height="55" alt="デザイン入稿について"></a></li>
      <li><a href="<?php bloginfo('url'); ?>/faq"><img src="<?php bloginfo('template_url'); ?>/img/common/header_nav04.jpg" width="145" height="55" alt="よくあるご質問"></a></li>
      <li><a href="http://rereca.com/about/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/common/header_nav05.jpg" width="145" height="55" alt="会社案内"></a></li>
      </ul>
      </nav>

      <div class="pull-right">
      <p class="pull-left pl30 pt20"><img src="<?php bloginfo('template_url'); ?>/img/common/header_campain.jpg" width="296" alt="版代・送料無料キャンペーン"></p>
      
      <div class="pull-right">
      <p class="mb5"><img src="<?php bloginfo('template_url'); ?>/img/common/header_tel.jpg" height="81" width="411" alt="電話番号:0120-838-117"></p>
      </div><!-- /pull-right -->
      
      <div class="pull-right">
      <p class="pull-left pr19 nmr5"><a href="<?php bloginfo('url'); ?>/contact"><img src="<?php bloginfo('template_url'); ?>/img/common/header_con.jpg" height="37" width="198" alt="お問い合わせはこちら"></a></p>
      <p class="pull-right"><a href="<?php bloginfo('url'); ?>/estimation"><img src="<?php bloginfo('template_url'); ?>/img/common/header_est.jpg" height="37" width="198" alt="お見積もりはこちら"></a></p>
      </div><!-- /pull-right -->
      </div><!-- /pull-right -->
      
      </div><!-- /col-xs-13 -->
		</div><!-- /row -->
	</div><!-- container-->
</header>


<?php get_template_part('part','gnavi'); ?>

	<?php if(is_home()) : ?>
  <?php get_template_part('part','mainview'); ?>				

  <?php else :?>			
  <section id="feature">
  <div class="clearfix container pt20 pb10 mb30">        
  <p><img alt="" src="<?php bloginfo('template_url'); ?>/img/common/h2_bg.jpg" /></p>
  
  <div class="wrapper header-content clearfix"> 
  <?php if(is_page('shape')) : ?>																				
  <h2 class="feature-title">STEP1 形状を選ぶ</h2>
  
  <?php elseif(is_page('example') || is_singular('post') || is_category() || is_month() || is_year() ) : ?>																			
  <h2 class="feature-title">制作事例集</h2>
  <?php elseif(is_404()) :?>
  <h2 class="feature-title">404エラー</h2>							
  <?php else :?>
  <h2 class="feature-title"><?php the_title(); ?></h2>							
  <?php endif; ?>
  </div><!-- ./header-content -->
  </div><!-- ./wrapper -->
  </section><!-- end feature -->
  <?php endif; ?>

