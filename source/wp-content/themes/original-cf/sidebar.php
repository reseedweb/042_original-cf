<div class="container">
<div class="row">
<div class="col-xs-5">

<div id="sidebar" class="pull-left">
<!--
<?php if(is_page('example') || is_singular('post') || is_category() || is_month() || is_year()) : ?>
  <div class="sidebar-row clearfix">
  <div class="sideBlog">
  <h4 class="sideBlog-title mt0"><i class="fa fa-bookmark"></i>最新の投稿</h4>
  <ul>
  <?php query_posts("posts_per_page=5"); ?><?php if(have_posts()):while(have_posts()):the_post(); ?>
  <li>
  <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
  </li>
  <?php endwhile;endif; ?>
  </ul> 
  </div>
  <div class="sideBlog mt10">
  <h4 class="sideBlog-title"><i class="fa fa-bookmark"></i>カテゴリー</h4>
  <ul class="category">
  <?php wp_list_categories('orderby=count&order=desc&show_count=1&title_li='); ?>
  </ul>                             	
  </div>
  <div class="sideBlog mt10">
  <h4 class="sideBlog-title"><i class="fa fa-bookmark"></i>月別アーカイブ</h4>
  <ul><?php wp_get_archives('type=monthly&limit=12'); ?></ul>                               	
  </div>
  </div>
<?php endif; ?>-->


  <div class="panel panel-primary side_contact">
  <p class="mb0"><img src="<?php bloginfo('template_url'); ?>/img/side_faq.jpg" alt="無料お見積もりはこちら"></p>
  <p class="mt10"><img src="<?php bloginfo('template_url'); ?>/img/side_tel.jpg" alt="電話:0120-838-117"></p>
  <p><a href="<?php bloginfo('url'); ?>/estimation"><img src="<?php bloginfo('template_url'); ?>/img/side_est.jpg" alt="お見積もりはこちら"></a></p>
  </div>

  <p><a href="<?php bloginfo('url'); ?>/fullorder">
  <img src="<?php bloginfo('template_url'); ?>/img/side_bnr01.jpg" alt="オリジナルクリアファイル"></a></p>
  <p><a href="<?php bloginfo('url'); ?>/nameorder">
  <img src="<?php bloginfo('template_url'); ?>/img/side_bnr02.jpg" alt="名入れ1色印刷クリアファイル"></a></p>
  <p><a href="<?php bloginfo('url'); ?>/price">
  <img src="<?php bloginfo('template_url'); ?>/img/side_bnr03.jpg" alt="参考価格例"></a></p>
 <!-- <p class="mb20"><a href="<?php bloginfo('url'); ?>/example">
  <img src="<?php bloginfo('template_url'); ?>/img/side_bnr04.jpg" alt="クリアファイル制作実績"></a></p>-->
	
  <div class="panel panel-primary mt20" id="accordion">
  <div class="panel-heading text-center">オーダーメニュー</div>
    <div class="list-group ml10 mr10 mb0">
    <a href="<?php bloginfo('url'); ?>/shape" class="list-group-item side_kami">
    <span>STEP1</span>形状を選ぶ<i class="fa fa-chevron-circle-right pull-right pr0"></i>
    </a>
    </div>
    <div id="collapseOne" class="panel-collapse collapse list-group in mt0">
      <a href="<?php bloginfo('url'); ?>/shape#1" class="list-group-item listItem01">
        <i class="fa fa-caret-square-o-right"></i>ノーマル
      </a>
      <a href="<?php bloginfo('url'); ?>/shape#2" class="list-group-item listItem01">
        <i class="fa fa-caret-square-o-right"></i>ポケット付き
      </a>
      <a href="<?php bloginfo('url'); ?>/shape#3" class="list-group-item listItem01">
        <i class="fa fa-caret-square-o-right"></i>ロングポケット付き
      </a>
      <a href="<?php bloginfo('url'); ?>/shape#4" class="list-group-item listItem01">
        <i class="fa fa-caret-square-o-right"></i>仕切り付き
      </a>
      <a href="<?php bloginfo('url'); ?>/shape#5" class="list-group-item listItem01">
        <i class="fa fa-caret-square-o-right"></i>封筒型ファイル縦
      </a>
      <a href="<?php bloginfo('url'); ?>/shape#6" class="list-group-item listItem01">
        <i class="fa fa-caret-square-o-right"></i>封筒型ファイル横
      </a>
      <a href="<?php bloginfo('url'); ?>/shape#7" class="list-group-item listItem01">
        <i class="fa fa-caret-square-o-right"></i>バッグ型ファイル縦
      </a>
      <a href="<?php bloginfo('url'); ?>/shape#8" class="list-group-item listItem01">
        <i class="fa fa-caret-square-o-right"></i>バッグ型ファイル横
      </a>
    </div>

    <div class="list-group ml10 mr10 mb0">
    <a href="<?php bloginfo('url'); ?>/size" class="list-group-item side_kami">
    <span>STEP2</span>サイズを選ぶ<i class="fa fa-chevron-circle-right pull-right pr0"></i>
    </a>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse list-group in mt0">
      <a href="<?php bloginfo('url'); ?>/size#1" class="list-group-item listItem01">
        <i class="fa fa-caret-square-o-right"></i>A4クリアファイル
      </a>
      <a href="<?php bloginfo('url'); ?>/size#2" class="list-group-item listItem01">
        <i class="fa fa-caret-square-o-right"></i>B5クリアファイル
      </a>
      <a href="<?php bloginfo('url'); ?>/size#3" class="list-group-item listItem01">
        <i class="fa fa-caret-square-o-right"></i>A6クリアファイル
      </a>
      <a href="<?php bloginfo('url'); ?>/size#4" class="list-group-item listItem01">
        <i class="fa fa-caret-square-o-right"></i>B6クリアファイル
      </a>
      <a href="<?php bloginfo('url'); ?>/size#5" class="list-group-item listItem01">
        <i class="fa fa-caret-square-o-right"></i>オリジナルサイズ
      </a>
    </div>

    <div class="list-group ml10 mr10">
    <a href="<?php bloginfo('url'); ?>/material" class="list-group-item side_kami">
    <span>STEP3</span>素材を選ぶ<i class="fa fa-chevron-circle-right pull-right pr0"></i>
    </a>
    </div>

    <div class="list-group ml10 mr10">
    <a href="<?php bloginfo('url'); ?>/print" class="list-group-item side_kami">
    <span>STEP4</span>印刷方法を選ぶ<i class="fa fa-chevron-circle-right pull-right pr0"></i>
    </a>
    </div>

    <div class="list-group ml10 mr10">
    <a href="<?php bloginfo('url'); ?>/option" class="list-group-item side_kami">
    <span>STEP5</span>オプションを選ぶ<i class="fa fa-chevron-circle-right pull-right pr0"></i>
    </a>
    </div>

    <div class="list-group ml10 mr10">
    <a href="<?php bloginfo('url'); ?>/price" class="list-group-item side_kami">
    <span>STEP6</span>参考価格例<i class="fa fa-chevron-circle-right pull-right pr0"></i>
    </a>
    </div>

  </div><!-- /panel -->


  <p><a target="_blank" href="http://original-pb.net"><img src="<?php bloginfo('template_url'); ?>/img/side_pb_banner.jpg" alt="オリジナル紙袋Web"></a></p>

  <p><a target="_blank" href="http://original-pb.net/plastic_bag/"><img src="<?php bloginfo('template_url'); ?>/img/side_bnr_pori.jpg" alt="ポリ手提げ袋 詳しくはこちら！"></a></p>

  <p class="mb20"><a href="<?php bloginfo('url'); ?>/about#novelty"><img src="<?php bloginfo('template_url'); ?>/img/side_bnr_novelty.jpg" alt="ノベルティグッズご相談＆制作できます!"></a></p>

  <div class="panel panel-primary">
    <div class="panel-heading text-center">ご利用案内</div>
    <div class="list-group">
    <a href="<?php bloginfo('url'); ?>/" class="list-group-item listItem01">
    <i class="fa fa-caret-square-o-right"></i>トップページ</a>
    <a href="<?php bloginfo('url'); ?>/about" class="list-group-item listItem01">
    <i class="fa fa-caret-square-o-right"></i>初めての方へ</a>
    <a href="<?php bloginfo('url'); ?>/flow" class="list-group-item listItem01">
    <i class="fa fa-caret-square-o-right"></i>ご注文の流れ</a>
    <a href="<?php bloginfo('url'); ?>/draft" class="list-group-item listItem01">
    <i class="fa fa-caret-square-o-right"></i>データ入稿について</a>
    <a href="<?php bloginfo('url'); ?>/faq" class="list-group-item listItem01">
    <i class="fa fa-caret-square-o-right"></i>よくあるご質問</a>
    <a href="http://rereca.com/about/" target="_blank" class="list-group-item listItem01">
    <i class="fa fa-caret-square-o-right"></i>会社案内</a>
    </div>
  </div>

</div><!-- /sidebar -->
</div><!-- /col-xs-5 -->